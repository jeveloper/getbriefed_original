GetBriefed.net/com
---------------

I built this startup to provide basic brief notes. Summary:

  - Built on codeigniter (php)
  - Completely ajax app, with backend providing json data 
  - Plenty of cool javascript plugins to select contacts and share Brief Notes
  - User interface experimentation with type short lines + hit enter and a brief note is complete, ready to be shared with your contacts