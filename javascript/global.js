/***************************** 
Copyright: Singens Inc jeveloper.com

************************/         

  function XNotify(message,code){

    //code  1 - success
    //code 2 - warning
    //code 3 - failure

    var bgcol = '#dfd'; //normal colour

    if (code == 2){
           bgcol = '#FFCC66';
    }
    else if (code == 3){
           bgcol = '#FFC6B3';
    }

      
     $('notification').show().update(message).setStyle({ background: bgcol });

      new Effect.Highlight('notification',
                    {
                        //startcolor: "#dfd",
                        //endcolor: "#FFC6B3",
                        //restorecolor: "#dfd",
                        duration: 1.5
                      }

      );

      //setTimeout("Effect.Fade('notification')",7000);
      Element.hide.delay(5, 'notification');


  }

  /***************************** GENERIC AJAX FUNCTIONS START  ************************/

  
  
  function onReceiveGenericDelete(request,opts){
   
  
        
   var response = request.responseText.split("\n");
            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                //Only now read the options
                
                 this.opts = {
                    id: 0,
                    reload: true
                    };
                    
                 Object.extend(this.opts, opts || { }); 
                
                
                //removes a row or element of a div
                $(this.opts.id).remove();
                
                if (this.opts.reload){
                //just reload
                  $("lst").fire("effect:loadup");     
                }
                
                    
                   $("lst").fire("effect:message",{msg: response[1],opt: 1}); 

                    return true;
                }else{
                  $("lst").fire("effect:message",{msg: response[1],opt: 3}); 
                    return false;
                }
            }

  }
   
  
  /***************************** GENERIC AJAX FUNCTIONS END  ************************/
  
  
  
  
  function toggleDiv(div,style) {
    Effect.toggle(div,style);
    return false;
  }
  
  function toggleDivs(divs) {
  divs.each(function(s) {
      Effect.toggle(s);
    });

    return false;
  }