
/*----------  Briefing Management -------------------*/

function addBriefing(srv){

    
     
    new Ajax.Request(srv, {
  method: 'POST',
  parameters: {t_title: $F('t_title'), tags: $F('tags'),body: $F('body'), privacy: $F('privacy')},
  onComplete:function(request){onReceiveBriefing(request)}
  });


}    

function clearAll(){
       //clean preview
                    $("b_point_list").update('');

                    //Clean form and preview
                    $("body").clear();
                    $("tags").clear();
                    $('t_title').clear();
                    $('b_point').clear();
} 
function onReceiveBriefing(request){


   var response = request.responseText.split("\n");


            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {
              
                      clearAll();
                    $("lst").fire("effect:message",{msg: response[1],opt: 1});   
                    $("lst").fire("effect:loadup",{privacy: $F('privacy')});
                    return true;
                }else{
                   $("lst").fire("effect:message",{msg: response[1],opt: 3});   
                    return false;
                }
            }

  }


  function onReceiveBriefingDelete(request,id){


   var response = request.responseText.split("\n");
            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                    $('briefing_'+id).remove();
                    $("lst").fire("effect:message",{msg: response[1],opt: 1});   

                    return true;
                }else{
                    $("lst").fire("effect:message",{msg: response[1],opt: 3});   
                    return false;
                }
            }

  }



  /*----------  Briefing Management END -------------------*/


 /*----------  Contact Management  -------------------*/
function addContact(srv){
        new Ajax.Request(srv, {
  method: 'POST',
  parameters: {t_fname: $F('t_fname'), t_lname: $F('t_lname'),t_email: $F('t_email')},
  onComplete:function(request){onReceiveContact(request)}
  });

  }


 function onReceiveContact(request){

   var response = request.responseText.split("\n");


            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                    //Clean form and preview
                    $("t_email").clear();
                    $("t_fname").clear();
                    $('t_lname').clear();

                     $("t_fname").focus();      
                    $("lst").fire("effect:loadup");
                    
                      $("lst").fire("effect:message",{msg: response[1],opt: 1});   

                    return true;
                }else{
                    $("lst").fire("effect:message",{msg: response[1],opt: 3});   
                    return false;
                }
            }

  }
    function onReceiveContactDelete(request){


   var response = request.responseText.split("\n");
            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                    $("lst").fire("effect:loadup");                     
                    $("lst").fire("effect:message",{msg: response[1],opt: 1});
                    return true;
                }else{
                    //XNotify(response[1],3);
                     $("lst").fire("effect:message",{msg: response[1],opt: 3});   
                    return false;
                }
            }

  }



  function loadupContacts(base_url,service_url,no_records){


//latest srvtype
var service = base_url+service_url;

new Ajax.Request(service, {
  method:'post',
  parameters: { },
  onSuccess: function(transport){
     var json = transport.responseText;
    
     var sb = new StringBuilder();
     $("lst").update('');



     if (json.isJSON()) {
      json = transport.responseText.evalJSON();


             if (json.length > 0){

             }else{
                
                 $("lst").fire("effect:message",{msg: no_records,opt: 1});   
             }
    
                        sb.append('<div class="light_rbroundbox"><div class="light_rbtop"><div></div></div><div class="rbcontent">');                                    
                        sb.append('<img src="#{base_url}img/toolicons/24-member.png" alt=""><br/>');            
                                    
                         //THIS IS a standard VCARD format
                         //http://microformats.org/code/hcard/creator                                 
                         sb.append("<div id='hcard-#{fname}-#{lname}' class='vcard'>");
                         sb.append("<span class='fn'>#{fname}&nbsp;#{lname}</span><br/>");
                         sb.append("<a class='email' href='mailto:#{email}'>#{email}</a>");
                         sb.append("</div>"); //END
                         sb.append("<p>Tools: <a href='#'   onclick=\"new Ajax.Request('#{base_url}subscriber/remove/#{id}',{onComplete:function(request){onReceiveContactDelete(request)}, evalScripts:true}); return false;\"    rel='external nofollow'><img src='#{base_url}img/toolicons/24-member-remove.png' border='0' alt='Delete'/></a></p>");                                   
            
            
                         sb.append('</div><div class="light_rbbot"><div></div></div></div>');
               
               var template = new Template(sb.toString());
             
               sb.clear();
            for(var i=0; i<json.length; i++)
            {
              sb.append(template.evaluate({id:json[i].id,fname:json[i].firstname,lname:json[i].lastname,email:json[i].email,base_url:base_url}));
            }//end for loop
              $('lst').insert(sb.toString());
               sb.clear();
               

            //Finished loading


     }else{
        //Empty
     }
   },


    onFailure: function(){
    
     $("lst").fire("effect:message",{msg: 'Unable to receive data, Try again.',opt: 2});   
    }


});


}


   /*----------  Contact Management END -------------------*/

  function addBPoint(){

  //Logic
  
  
  //Implementation
                    
                var txt = $("b_point").getValue();
                var body = $('body');
                  //variables
                var objList = $('b_point_list');
               
                                  
                if (txt.strip() != ""){

                    $('b_point_list').insert("<li>"+txt.truncate(30,'...')+"</li>");
                    body.setValue( txt + '<sp>'   + body.getValue()) ;

                    $("b_point").setValue('');

                    new Effect.Highlight('b_point_list');

                    //SET focus back to initial field
                     $("b_point").focus();

                }else if ($F('body').empty() == false && txt.strip() == ""){
                   //add a note when at least one is added, quicker
                    addBriefing(add_briefing_url,'send_btn','send_status');
                }
               
  }


   //Handler for Select Contacts link 
   //sets the hidden value of the briefing id that was clicked on
     function handler(e) {
     
         var data = $A(arguments);
         data.shift();
         
         $("bid_selected").value = this.bid;
         Modalbox.show($("select_contacts"), {title: "Select Contacts  And Send", height: 300,width: 500});  return false;
     }
 
  function loadupBriefings(base_url,service_url,no_records){
       
    new Ajax.Request(service_url, {
      method:'post',
      parameters: { },
      onSuccess: function(transport){
         var json = transport.responseText;
         var str = '';
     
     
     var sb = new StringBuilder();
    
     
     $("lst").update('');
    


     if (json.isJSON()) {
      json = transport.responseText.evalJSON();

           
             if (json.length > 0){             
               $("lst").fire("effect:message",{msg: 'Brief Notes: '+json.length,opt: 1});     
             }else{                
                $("lst").fire("effect:message",{msg: no_records,opt: 2}); 
                Modalbox.show($("first_time"), {title: "First time?", height: 400,width: 600});  return false;     
             }
                                                   
                  
                var del = '<cite><a href="#"   onclick="new Ajax.Request(\'#{base_url}briefing/remove/#{id}\',{onComplete:function(request){onReceiveBriefingDelete(request,#{id})}, evalScripts:true}); return false;"    rel="external nofollow"><img src="#{base_url}img/toolicons/16-em-cross.png" border="0" alt="Delete"/></a></cite>';  
                
            
                
                //node below is an actual reference 
                var SendContacts = '<cite><a href="#" id="selectContacts_#{id}"   title="Select Contacts And Send" ><img src="#{base_url}img/toolicons/email_go.png" border="0" alt="Send"/></a></cite>';
                 
                 
                  
                sb.append('<div id="briefing_#{id}">');
                sb.append('<div id="briefing_section">');
                sb.append('<div class="gravatar"><p>#{dcreated}');                
                sb.append(del);
                sb.append(SendContacts);
                
                sb.append('#{lastsent}');
                sb.append('</div>');
                                 
                sb.append("<div class=\"gText\" > <a id='tags_#{id}'>#{tags}</a> |  Title: <a id='title_#{id}'>#{title}</a>");       
                sb.append("| <img border='0' alt=''   src='#{base_url}#{img_privacy}'/> ");                                                      sb.append(" | <a href='briefing/sview/#{id}'>Single view</a>");             
                sb.append("</div>");
                sb.append("<ol>");
                sb.append('#{points}');                
                sb.append("</ol>");
                sb.append("<p id=\"footer\">");
                sb.append('</p></div>'); //briefing_section 
                sb.append("</div>");//briefing_XX  
                
                
                  
                var template = new Template(sb.toString());             
                sb.clear();

                
                
            for(var i=0; i<json.length; i++)
            {
                var img_privacy = '';
            //private, public , group
                if (json[i].privacy == 'private'){
                    img_privacy= 'img/toolicons/key.png';
                }
                else if (json[i].privacy == 'public'){
                    img_privacy= "img/toolicons/transmit.png";
                }
                else if (json[i].privacy == 'group'){
                    img_privacy= "img/toolicons/group.png";
                }
                
                
                var points =  json[i].body.splitOnSections();
                var pts = '';
                points.without('').each(function(item){
                        pts += "<li>"+item+"</li>";
                });
                
                var lastsent = '';
                if (json[i].lastsent != null){
                    lastsent = '<br/>Sent '+humane_date(json[i].lastsent);
                }                       
                
                 $('lst').insert(
            template.evaluate(
            {id:json[i].id,dcreated:humane_date(json[i].datecreated),
            img_privacy:img_privacy,
            points:pts,
            tags:json[i].tags,
            base_url:base_url,
            title:json[i].title,
             lastsent:lastsent}
            )); 
                
               //Register In place editor
               
               new Ajax.InPlaceEditor('tags_'+json[i].id, base_url+'briefing/updatetags/'+json[i].id,
               {
                   submitOnBlur: true, callback: function(form, value) { return 'tags=' + value }
               } )

                new Ajax.InPlaceEditor('title_'+json[i].id, base_url+'briefing/updateTitle/'+json[i].id,
               {
                   submitOnBlur: true, callback: function(form, value) { return 't_title=' + value }
               } )

                //Register select contacts
                var obj = { bid: json[i].id };
                Event.observe('selectContacts_'+json[i].id, 'click', handler.bindAsEventListener(obj));

              
            }//end for loop
               
               
            //Finished loading


     }else{
        //Empty
     }
   },


    onFailure: function(){
    
      $("lst").fire("effect:message",{msg: 'Unable to receive data, Try again',opt: 3});     
           
    }


});


}

String.prototype.splitOnSections = String.prototype.split.curry("<sp>");
function hide_section($id){      Effect.BlindUp($id); }
function show_section($id){      Effect.BlindDown($id); }
function alterType(ntype){


    for ( i=0;i<10;i++){
            var link = $('lst').down('ol',i);
            if (link != undefined){
                link.writeAttribute('type', ntype);
            }else{
                break;
            }
    }
}
function alterSrvType(ntype){ $('srvType').update(ntype);  $("lst").fire("effect:loadup"); }

 //var link = new Element('a', { href : 'http://www.example.com' });
 //http://www.phpriot.com/articles/prototype-element-extensions/5

 

  



 function unloadBriefings(base_url,s_url,main_content_id,no_records){
      
      //initally load it
      loadupBriefings(base_url,s_url,no_records);

      //register event to fire messages
          document.observe("effect:message", function(event) {
              XNotify(event.memo.msg,event.memo.opt);
              
        });

      //Registering events
        document.observe("effect:loadup", function(event) {
        
        //Optionally this can be set
        if (event.memo.privacy != null){
            s_url = "briefing/loadbyprivacy/"+event.memo.privacy;
           
        }
            loadupBriefings(base_url,s_url,no_records);
        });
        
        
  }

  function unloadContacts(base_url,main_content_id,service_url,no_records){
      
      //initally load it
      loadupContacts(base_url,service_url,no_records);

      
        //register event to fire messages
         document.observe("effect:message", function(event) {             
              XNotify(event.memo.msg,event.memo.opt);         
        });

      //Registering events
        document.observe("effect:loadup", function(event) {
            loadupContacts(base_url,service_url,no_records);
        });
                 
  }
  
  
  
    


  /***************************** Contact select and management ************************/
  //an CSV array for contact ids
  var cids ="";
  
  function VisualSelect(id){
    if ($(id).getOpacity() == 1){
    //if select
      $(id).setOpacity(0.5);
      
          if (cids == ""){
          //initial
                cids = id;
          }else{
            cids += ',' + id ;
          }
          
    }
     else{
     //if deselect
        $(id).setOpacity(1);
        
        //remove from the list
         cids.sub(','+id, '');

     }

        
        
  }
  
  function sendQuickBriefing(srv,btn_id,send_status){

  if (cids.empty() == false){
        new Ajax.Request(srv, {
      method: 'POST',
      parameters: {ids: cids,bid: $("bid_selected").value},
      onComplete:function(request){onReceiveBsent(request,btn_id,send_status)}
      });
      cids = '';

  }
 }
 
  function onReceiveBsent(request,btn_id,send_status){


   var response = request.responseText.split("\n");
            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                   $(btn_id).hide();
                   $('body').setValue('');//cleanup hidden body
                   $(send_status).update(response[1]);  //shows a message in the window too
                   Modalbox.hide();//close the selection
                     $("lst").fire("effect:message",{msg: response[1],opt: 1});  

                    return true;
                }else{
                    $("lst").fire("effect:message",{msg: response[1],opt: 3});  
                    return false;
                }
            }

  }
  
  
  /***************************** Contact select and management  END  ************************/
  
  

   