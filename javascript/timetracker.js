  /***************************** HOURS management  START  ************************/
  
  /*
  JS learn
  var Bookmark = Class.create({
initialize: function(name, url) {
this.name = name;
this.url = url;
},
toHTML: function() {
return '<a href="#{url}">#{name}</a>'.interpolate(this);
}
});
var api = new Bookmark('Prototype API', 'http://prototypejs.org/api');
Object.toHTML(api);
//-> '<a href="http://prototypejs.org/api">Prototype API</a>'
  
  
  */
  
  
  function unloadHours(base_url,main_content_id,service_url,no_records){
      
      //initally load it
      loadupHours(base_url,service_url,no_records);
        
         //register event to fire messages
          document.observe("effect:message", function(event) {
              XNotify(event.memo.msg,event.memo.opt);
              
        });
        
      //Registering events
        document.observe("effect:loadup", function(event) {
            loadupHours(base_url,service_url,no_records);
        });
  }
  
  
  
  
     function addHour(srv){
        new Ajax.Request(srv, {
  method: 'POST',
  parameters: {t_date: $F('t_date'), t_hours: $F('t_hours'), t_desc: $F('t_desc'),t_isbill: $F('t_isbill')},
  onComplete:function(request){onReceiveHour(request)}
  });

  }


 function onReceiveHour(request){


   var response = request.responseText.split("\n");


            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                    //Clean form and preview
                    $("t_date").clear();
                    $("t_hours").clear();
                    $("t_desc").clear();
                    $("t_isbill").clear();
                    
                     //HIDE the form if they selected Add not Add ++
                     
                   
                    $("lst").fire("effect:message",{msg: response[1],opt: 1}); 
                     $("lst").fire("effect:loadup");    
                    return true;
                }else{
                   $("lst").fire("effect:message",{msg: response[1],opt: 3}); 
                    return false;
                }
            }

  }
    



  function loadupHours(base_url,service_url,no_records){

    
//latest srvtype
var service = base_url+service_url;

new Ajax.Request(service, {
  method:'post',
  parameters: { },
  onSuccess: function(transport){
     var json = transport.responseText;
     
       //clearing the initial form
       //we can remove only one at a time, its nicer, add item too without data reload
     //$("lst").update('');

       $('lst').select('tr[id^=hour_]').each(function(item){
                        item.remove();
                });

     if (json.isJSON()) {
     
     
      json = transport.responseText.evalJSON();


             if (json.length > 0){
                  
             }else{
             
                $("lst").fire("effect:message",{msg: no_records,opt: 2}); 

             }
             
              // define the template used to display each contact
          var sb = new StringBuilder(); 
          
      var del = '<cite><a href="#"   onclick="new Ajax.Request(\'#{base_url}hours/remove/#{id}\',{onComplete:function(request){onReceiveGenericDelete(request,{id:\'hour_#{id}\',reload:false})}, evalScripts:true}); return false;"    rel="external nofollow"><img src="#{base_url}img/toolicons/16-em-cross.png" border="0" alt="Delete"/></a></cite>';       
        var template = new Template(
            '<tr id="hour_#{id}">'
          + '    <td>#{date}</td>'
          + '    <td>#{hours}</td>'
          + '    <td>#{desc}</td>'
          + '    <td>#{isbill}</td>'  
          + del        
          + '</tr>'
        );
        

             var str = '';
            for(var i=0; i<json.length; i++)
            {                
                  sb.append(template.evaluate(
                  {id:json[i].id,date: json[i].date,hours: json[i].hours,
                  base_url: base_url,
                  desc: json[i].description,isbill:json[i].isbill}));
                              
            }//end for loop
            
               $('addForm').insert({after: sb.toString()});
              
                 sb.clear(); 
               
                 /* TO REUSE
                 $('myRow1').insert({
  after: '<tr><td>#{name}</td></tr>'.interpolate({
    name: 'ABC'
  })
}) 
                 */
               

            //Finished loading


     }else{
        //Empty
     }
   },


    onFailure: function(){
    $("lst").fire("effect:message",{msg: 'Unable to receive data.',opt: 2}); 
    }


});


}
  
  /***************************** Hour management END  ************************/
  
    /***************************** Project Management  START  ************************/
  
  function unloadProjects(base_url,main_content_id,service_url,no_records){
      
      //initally load it
      loadupProjects(base_url,service_url,no_records);

       //register event to fire messages
         document.observe("effect:message", function(event) {
              XNotify(event.memo.msg,event.memo.opt);
              
        });

      //Registering events
       document.observe("effect:loadup", function(event) {
            loadupProjects(base_url,service_url,no_records);
        });
  }
  
     function addProject(srv){
        new Ajax.Request(srv, {
  method: 'POST',
  parameters: {t_name: $F('t_name'), t_fee: $F('t_fee')},
  onComplete:function(request){onReceiveProject(request)}
  });

  }


 function onReceiveProject(request){


   var response = request.responseText.split("\n");


            if ( response.length == 2 )
            {
                if ( response[0] == 'OK' && response[1] )
                {

                    //Clean form and preview
                    $("t_name").clear();
                    $("t_fee").clear();
                    

                     $("t_name").focus();      
                    
                    $("lst").fire("effect:message",{msg: response[1],opt: 1}); 
                    $("lst").fire("effect:loadup");
                    return true;
                }else{
                    $("lst").fire("effect:message",{msg: response[1],opt: 3}); 
                    return false;
                }
            }

  }
    



  function loadupProjects(base_url,service_url,no_records){


//latest srvtype
var service = base_url+service_url;

new Ajax.Request(service, {
  method:'post',
  parameters: { },
  onSuccess: function(transport){
     var json = transport.responseText;
     
      var sb = new StringBuilder();
     $("lst").update('');



     if (json.isJSON()) {
      json = transport.responseText.evalJSON();


             if (json.length > 0){
                 $("lst").fire("effect:message",{msg: 'Showing project: '+json.length,opt: 1});  
             }else{
                $("lst").fire("effect:message",{msg: no_records,opt: 2}); 

             }
             
                    sb.append('<div class="light_rbroundbox"><div class="light_rbtop"><div></div></div><div class="rbcontent">');
                        sb.append('<p>');            
                       sb.append("<a  id='t_name_#{id}'>#{name}</a>&nbsp;");   
                       sb.append('&#64;<strong>#{fee}/hour</strong>');     //this is  @10/hour
                       sb.append("<h3 class='bill'>#{billhours}</h3><span class='hoursTxt'>hrs.</span>&nbsp;<h4 class='nonbill'>#{nonbill}</h4><span class='hoursTxt'>hrs.</span>");
                        
                     
     sb.append("<br/>Tools: <a href='#{base_url}/project/timesheet/#{id}'>Enter Hours</a> <a href='#'   onclick='new Ajax.Request('#{base_url}project/remove/#{id}',{onComplete:function(request){onReceiveGenericDelete(request)}, evalScripts:true}); return false;'    rel='external nofollow'><img src='#{base_url}img/toolicons/16-em-cross.png' border='0' alt='Delete'/></a>");                                   
                       sb.append('</p>');
            
                        sb.append('</div><div class="light_rbbot"><div></div></div></div>');
                 
                 
             var template = new Template(sb.toString());
             sb.clear();    
            
            for(var i=0; i<json.length; i++)
            {  
               
                 $('lst').insert(template.evaluate({
                 id:json[i].id, name: json[i].name, base_url: base_url, nonbill:json[i].nonhours, billhours: json[i].billhours, fee: json[i].fee
                 })); 
                
                  
                new Ajax.InPlaceEditor('t_name_'+json[i].id, base_url+'project/updateName/'+json[i].id,
               {
                   submitOnBlur: true, callback: function(form, value) { return 't_name=' + escape(value) }
               } )
               
               
               
            }//end for loop
            
        
            
               

               

            //Finished loading


     }else{
        //Empty
     }
   },


    onFailure: function(){
  $("lst").fire("effect:message",{msg: 'Try again.',opt: 1}); 
    }


});


}
  
  /***************************** Project Management  END  ************************/
  