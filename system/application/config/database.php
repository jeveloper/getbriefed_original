<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|    ['hostname'] The hostname of your database server.
|    ['username'] The username used to connect to the database
|    ['password'] The password used to connect to the database
|    ['database'] The name of the database you want to connect to
|    ['dbdriver'] The database type. ie: mysql.  Currently supported:
                 mysql, mysqli, postgre, odbc, mssql
|    ['dbprefix'] You can add an optional prefix, which will be added
|                 to the table name when using the  Active Record class
|    ['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|    ['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|    ['cache_on'] TRUE/FALSE - Enables/disables query caching
|    ['cachedir'] The path to the folder where cache files should be stored
|    ['char_set'] The character set used in communicating with the database
|    ['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = "hosting";
$active_record = TRUE;

//http://codeigniter.com/wiki/MySQL_4.0/




$db['hosting']['hostname'] = "localhost";
$db['hosting']['username'] = "ad";
$db['hosting']['password'] = "";
$db['hosting']['database'] = "";
$db['hosting']['dbdriver'] = "mysqli";
$db['hosting']['dbprefix'] = "";
$db['hosting']['pconnect'] = TRUE;
$db['hosting']['db_debug'] = TRUE;
$db['hosting']['cache_on'] = FALSE;
$db['hosting']['cachedir'] = "";
$db['hosting']['char_set'] = "utf8";
$db['hosting']['dbcollat'] = "utf8_general_ci";
$db['hosting']['port'] = "3306"; 



$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "";
$db['default']['database'] = "getbriefed";
$db['default']['dbdriver'] = "mysqli";
$db['default']['dbprefix'] = "getbriefed_";
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";
$db['default']['port'] = "3306"; 

?>