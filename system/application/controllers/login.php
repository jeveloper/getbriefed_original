<?php
  class Login extends Controller{

      function Login(){
        parent::Controller();
         
      }

    function index()
    {
        
         $this->load->helper('cookie');  
         
         if (get_cookie('gb_rememberme') != FALSE){
         //skip through logic page
             if (get_cookie('gb_user_email') != FALSE){
                    $email = $this->encrypt->decode(get_cookie('gb_user_email'));
                    $this->processByEmail($email);
                    return;
            }
         }
              $this->view();
    }

    function view(){

        //Special case for Demo account
        
      
        
        $email = $this->input->post('email');
         $password = '';
         
          $segment = $this->uri->segment(3);  
        
       
          
        //load from cookie
        $this->load->helper('cookie');
        if (get_cookie('gb_user_email') != FALSE){
                $email = $this->encrypt->decode(get_cookie('gb_user_email'));
        }
            $data = array(
               'email' => $email,'password' => ''
          );
          
          
          //DEMO account passes through
           if ($segment == 'demo'){
             $email = 'demo@demo.com';             
             $this->processByEmail($email);
        
        }

        $this->load->view("login",$data);

    }





    function logoff(){
         //Loggin off
         //clearing session
         //clear off remember me
         //redirect to main site
         $this->load->helper('cookie'); 
        $this->session->sess_destroy();
        delete_cookie('rememberme');
        redirect("/welcome");
    }

    
    function processByEmail($email){
        $this->load->model('Usermodel','umodel');
                $result = $this->umodel->getUserByEmail($email);

                if (isset($result) && $result != null){

                    $row = $result[0];
                    $array_items = array('userid' => $row->id, 'email' => $row->email, 'fname' => $row->firstname, 'lname'=>$row->lastname);
                    $this->session->set_userdata($array_items);
                    redirect("/briefing/");                 
                }
    }
    function process(){

         $rules['email']    = "required";
         $rules['password']    = "required";

  
         
            $this->validation->set_rules($rules);
             if ($this->validation->run() == FALSE){
                 $this->validation->error_string = 'Please enter email/password.';
             }else{

                $this->load->model('Usermodel','umodel');
                $result = $this->umodel->getUser($this->input->post('email'),$this->input->post('password'));

                if (isset($result) && $result != null){

                    $row = $result[0];
                    $array_items = array('userid' => $row->id, 'email' => $row->email, 'fname' => $row->firstname, 'lname'=>$row->lastname);
                    $this->session->set_userdata($array_items);




                    //Remember Me
                    if ($this->input->post('rem') == "checked"){
                          $this->load->helper('cookie');
                            $cookie = array(
                           'name'   => 'user_email',
                           'value'  =>  $this->encrypt->encode($this->input->post('email')),
                           'expire' => '1209600'
                       );

                     set_cookie($cookie);

                         $this->load->helper('cookie');
                            $cookie = array(
                           'name'   => 'rememberme',
                           'value'  => 'true',
                           'expire' => '1209600'
                       );

                     set_cookie($cookie);

                     
                    }


                      redirect("/briefing/");

                }else{
                    // is not authenticated
                     $this->validation->error_string = 'Email/password do not match our records';
                     $this->view();
                }

        }//not empty fields
    }


  }
?>