<?php
/**********
*Copyright Singens Consultance
*
*Subscriber Controller handles everything related to Management of Contacts and usage of Contacts by other controllers/views
*Used directly by a View
*
*
*/
  class Subscriber extends Controller{



      function Subscriber(){
         parent::Controller();
      }

      function index(){
          $this->loginmodel->checklogin();
        $this->lang->load('subscriber','english');
        $this->load->view("manage_subscribers");
        
      }


      //Adding a Contact (returns the regular OK, FAIL)
      function add(){
            $this->lang->load('subscriber','english');
                //will add a contact
                 $rules['t_fname']    = "required";
                 $rules['t_email']    = "required|valid_email";

            $this->validation->set_rules($rules);
             if ($this->validation->run() == TRUE){
                 $this->load->model('Contactmodel','cmodel');
                 $this->cmodel->insert($this->input->post('t_email'),$this->input->post('t_fname'), $this->input->post('t_lname'),$this->session->userdata('userid'));
                 echo "OK\n".$this->lang->line("added");
        }else{
            echo "FAIL\n".$this->lang->line('incomplete_form');;
        }
      }

      //Removal of contact
      function remove(){
            $br_id = $this->uri->segment(3);
           $this->load->model('Contactmodel','cmodel');
           $this->cmodel->remove($br_id);

          $this->lang->load('subscriber','english');
          echo "OK\n".$this->lang->line("deleted");
      }


   

      //This Resource produces the result of JSON type (see table Contacts)
       function getContacts(){
          $this->load->model('Contactmodel','cmodel');
          $result = $this->cmodel->getContacts($this->session->userdata('userid'));
                echo json_encode($result);
          //encode even an empty array
    }

    function selectcontacts(){
        
        if ($this->uri->segment(3) != ''){
        $this->load->model('Contactmodel','cmodel');
        $this->lang->load('subscriber','english');
        $result = $this->cmodel->getContacts($this->session->userdata('userid'));
        $data = array("result"=>$result,'briefingid'=>$this->uri->segment(3));

        
        
        $this->load->view("selectcontacts",$data);
        }
    }
    
    function searchShort(){
               //expect a POST variable
               
        $str = "<ul>";
        for($i =0;$i<10;$i++){
                    $str .= "<li>".$i."</li>";
        }
        echo $str.'</ul>';
    }

  }
?>
