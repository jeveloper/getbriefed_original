<?php
   class Registration extends Controller{


      function Registration(){
        parent::Controller();
      }


    function index(){
        $this->view();
    }

    function view(){
        $this->load->plugin('captcha');





        srand((float) microtime() * 10000000);
        $input = array("NEON", "TAXI", "SUB", "JAVA", "SUN", "DAY","BLUE");
        $rand_keys = array_rand($input, 2);


      $vals = array(
                    'word' => $input[$rand_keys[0]],
                    'img_path'     => 'captcha/',
                    'font_path'     => './fonts/texb.ttf',
                    'img_url'     => $this->config->item('base_url').'captcha/',
                    'img_width'     => '150',
                    'img_height' => 30
                );


      $cap = create_captcha($vals);


      $data = array(
               'captcha' => $cap['image'],
               'email' => $this->input->post('email'), 'fname' =>$this->input->post('firstname'), 'lname'=>$this->input->post('lastname')
          );



      $this->session->set_userdata('captcha_word',$cap['word']);





     $this->load->view("register",$data);
    }





    function process(){


            $rules['firstname']    = "required";
            $rules['lastname']    = "required";
            $rules['password']    = "required|matches[password2]";
            $rules['password2']    = "required";
            $rules['email']        = "required|valid_email";

            $this->validation->set_rules($rules);



            if (strtolower($this->session->userdata('captcha_word')) !=  strtolower($this->input->post('captcha'))){
                $this->validation->error_string = 'Characters in the box do not match.';
                $this->view();
                return 0;

            }

             if ($this->validation->run() == FALSE){
                 $this->validation->error_string = 'Please fill out all required fields.';
                $this->view();


            }else{
             $this->load->model('Usermodel','umodel');

             if ($this->umodel->isEmailUsed($this->input->post('email'))){
                    $this->validation->error_string = 'Email you selected is already used.';
                    $this->view();
             }else{

                 //CLEAR any session data that was there
                  $this->session->sess_destroy();

                $userid = $this->umodel->insert_user($this->input->post('email'),$this->input->post('password'),$this->input->post('firstname'), $this->input->post('lastname'));

                  $array_items = array('userid' => $userid, 'email' => $this->input->post('email'), 'fname' =>$this->input->post('firstname'), 'lname'=>$this->input->post('lastname'));
                  $this->session->set_userdata($array_items);

                redirect("/briefing/");


             }
            }



      }//end of function

   }
?>