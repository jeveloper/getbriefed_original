<?php
  class Project extends Controller {
  
  
      
      
      function Project(){
        parent::Controller();
      }
      
      
      //VIEWS
      
    
      
      function index(){
          $this->loginmodel->checklogin(); 
          $this->lang->load('project','english'); 
        $this->load->view("timex/projects");
      }
      
      function timesheet(){
          //$this->loginmodel->checklogin(); 
           $this->lang->load('hour','english'); 
         $this->load->view("timex/timeentry");     
      }
      
      
      
      
      //Ajax called
      
      //Loads and echos json structure of all projects by creatorid for this user
      function loadMyProjects(){
            $option = $this->uri->segment(3);
                      
            
          $this->load->model('Projectmodel','pmodel');
          if ($option == 'full'){
          $result = $this->pmodel->findbyCreatorFull($this->session->userdata('userid') );
          }else{
          $result = $this->pmodel->findbyCreator($this->session->userdata('userid') );
          }
           echo json_encode($result);                     
      }
      
      function add(){
        //add a new project
        
    
        
          $ownerid =  $this->session->userdata('userid');
                //add all fields
              
              //FEE and NAME
           
           //front end controls validation too     
                
            $this->lang->load('project','english');
               
                 $rules['t_fee']    = "required";
                 $rules['t_name']    = "required"; 
                 
                 
            $this->validation->set_rules($rules);
             if ($this->validation->run() == TRUE){
                 $this->load->model('Projectmodel','pmodel');
                                                                                                          
                 $this->pmodel->create($this->input->post('t_name'),$this->input->post('t_fee'),$ownerid);
                 
                 
                 echo "OK\n".$this->lang->line("added");
        }else{
            echo "FAIL\n".$this->lang->line('incomplete_form');;
        }
        
      }
      
      function remove(){
      //remove project
       $id = $this->uri->segment(3);
          $this->load->model('Projectmodel','pm');
          
          $this->pm->delete($id);
              $this->lang->load('project','english');   
             echo "OK\n".$this->lang->line("deleted");
      }
      
      function updateName(){
             $id = $this->uri->segment(3);
            if ($id > 0){
             
                if ($this->input->post("t_name") != ""){

                     $this->load->model('Projectmodel','pmodel');
                                           
                      $this->pmodel->updateName($id,$this->input->post('t_name'));
                        
                }
            }

        echo $this->input->post("t_name");
      }
      function updateFee(){
                $id = $this->uri->segment(3);
            if ($id > 0){
             
                if ($this->input->post("t_fee") != ""){

                     $this->load->model('Projectmodel','pmodel');
                                           
                      $this->pmodel->updateFee($id,$this->input->post('t_fee'));

                }
            }

        echo $this->input->post("t_fee");
      
      }
      
      function  printBillHours(){
          $total = 0;
          
          $projectid =  $this->uri->segment(3); 
         if (is_numeric($projectid)){
               $this->load->model('Hourmodel','hmodel');
              $total = $this->hmodel->getTotalBillableHours($projectid);
         }
          if ($total == null) $total = 0; 
         echo $total;
      }
      
          function  printNonBillHours(){
          $total = 0;
          $projectid =  $this->uri->segment(3); 
         if (is_numeric($projectid)){
               $this->load->model('Hourmodel','hmodel');
               $total =  $this->hmodel->getTotalNonBillableHours($projectid);
         }
         if ($total == null) $total = 0;
         
         echo $total;
      }
      
      
    
      
      
      
      
  }
?>
