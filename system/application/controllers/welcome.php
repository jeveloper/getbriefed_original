<?php
   class Welcome extends Controller{



    function Welcome(){
        parent::Controller();
    }

     function index()
    {
        //Process for remember me features
        $this->load->helper('cookie');  
         
         if (get_cookie('gb_rememberme') != FALSE){
            //skip through first page
             if (get_cookie('gb_user_email') != FALSE){
                redirect("/login");
                    
            }
         }
        
         $this->view();       

    }

    function view(){
        $this->load->view("welcome");
    }

    function about(){
        $this->load->view("about/about");
    }
    
    function learn(){
        $this->load->view("about/learn");
    }
    
    function contact(){
        $this->load->view("about/contact");
    }
    
    function changes(){
        $this->load->view("about/changes");
    }


}
?>
