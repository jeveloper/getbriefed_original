<?php
  class Hours extends Controller {
  
  
      
      function Hours(){
        parent::Controller();
         
      }
      
      
      //VIEWS
    
      
      function index(){
        $this->load->view("hours");
      }
      
      
      
      
      //Ajax called
      
       function add(){
        //need projectid
        $projectid =  $this->uri->segment(3);  
      
           
           
           //front end controls validation too     
                
            $this->lang->load('hour','english');
               
                 $rules['t_date']    = "required";
                 $rules['t_hours']    = "required";                 
               

            $this->validation->set_rules($rules);
             if ($this->validation->run() == TRUE){
                 $this->load->model('Hourmodel','hmodel');
                 
                 $isbill = $this->input->post('t_isbill');
                  if ($isbill == 'on'){
                        $isbill = '1';
                  }else{
                        $isbill = '0';
                  }
                 
                 //format of the date ?
                 
                 $this->hmodel->create($projectid, $this->input->post('t_date'), $this->input->post('t_hours'),$isbill,$this->input->post('t_desc'));
                 
                 
                 echo "OK\n".$this->lang->line("added");
        }else{
            echo "FAIL\n".$this->lang->line('incomplete_form');;
        }
                
                
       }
       
       //expect id 
       function remove(){
          $id = $this->uri->segment(3);
           $this->load->model('Hourmodel','hmodel');
           $this->hmodel->delete($id);

          $this->lang->load('hour','english');
          echo "OK\n".$this->lang->line("deleted");
       
       }
       
       //Edit Hours   for particular row
       
       function editHours(){
       $id = $this->uri->segment(3);
            if ($id > 0){
             
                if ($this->input->post("t_hours") != ""){

                     $this->load->model('Hourmodel','hmodel');
                     
                      $this->hmodel->updateHours($id,$this->input->post("t_hours"));

                }
            }

        echo $this->input->post("t_hours");
       
       }
       
       //edit description for a particular row
       
       function editDesc(){
           $id = $this->uri->segment(3);
            if ($id > 0){
             
                if ($this->input->post("t_desc") != ""){

                     $this->load->model('Hourmodel','hmodel');
                     
                      $this->hmodel->updateHours($id,$this->input->post("t_hours"));

                }
            }

        echo $this->input->post("t_desc");
             
       }
       
       
       //Expect a project id 
         function gethours(){
              $id = $this->uri->segment(3);  //projectid
              
              //TODO  check agianst session.projects[i] if it matches any
              
              if (is_numeric($id)){
                    $this->load->model('Hourmodel','hmodel'); 
                    $result = $this->hmodel->findbyProject($id);
                    echo json_encode($result);  
              }
              
              
         }
      
      
  }
?>
