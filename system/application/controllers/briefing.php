<?php
   class Briefing extends Controller{



    function Briefing(){
        parent::Controller();
    }



     function index()
    {
      
      $this->loginmodel->checklogin();
     
        $this->dashboardView() ;
    }

    function dashboardView(){
        $this->lang->load('briefing','english');
        
        $this->load->model('Contactmodel','cmodel');
        
        $result = $this->cmodel->getContacts($this->session->userdata('userid'));
        $data = array("contacts"=>$result,'useridEnc'=>$this->encrypt->encode($this->session->userdata('userid')));

        
        
        $this->load->view("dashboardx",$data);
    }

    function DSview(){
        $this->load->view("s_dashboard");
    }

    //helper fun
    function isok($name){

        if ($this->input->post($name) != "")
            return true;
        else
            return false;

    }

    // receives POST request
    function add(){

        $this->lang->load('briefing','english');
         $rules['t_title']    = "required";
         $rules['body']    = "required";

            $this->validation->set_rules($rules);
       if ($this->validation->run() == TRUE){

                 $this->load->model('Briefingmodel','bmodel');
                 $this->bmodel->insert_entry($this->input->post('tags') ,$this->input->post('t_title'),$this->input->post('body'),
                 $this->session->userdata('userid') ,$this->input->post('privacy'));

            echo "OK\n".$this->lang->line("created_briefing");
        }else{
             echo "FAIL\n".$this->lang->line('incomplete_fields');;
        }
    }

    function remove(){
        $br_id = $this->uri->segment(3);
          $this->load->model('Briefingmodel','bmodel');

          //TODO if settings for this user/group is set to REMOVE use this
          //$this->bmodel->delete_entry($br_id);
          //by default it disables the briefing
          $this->bmodel->close_entry($br_id);

          $this->lang->load('briefing','english');
          echo "OK\n".$this->lang->line("briefing_removed");
    }

    //Updates the whole briefing
    function update(){
        //update_entry
         $this->load->model('Briefingmodel','bmodel');
         $this->bmodel->update_entry($this->input->post("id"),$this->input->post("tags"),$this->input->post("body"));
           $this->lang->load('briefing','english');
          echo "OK\n".$this->lang->line("briefing_updated");
    }

    //pass segement 3 and return the new value from post
    function updatetags(){
            $br_id = $this->uri->segment(3);
            if ($br_id > 0){
             

                if ($this->isok("tags") ){

                     $this->load->model('Briefingmodel','bmodel');
                     $this->bmodel->update_tags($br_id,$this->input->post("tags") );

                }
            }

        echo $this->input->post("tags");
    }

    function updateTitle(){
       $br_id = $this->uri->segment(3);
      if ($br_id > 0){
             
         if ($this->isok("t_title")){


             $this->load->model('Briefingmodel','bmodel');
             $this->bmodel->update_title($br_id,$this->input->post("t_title") );


            }

        }

        echo $this->input->post("t_title");
    }

//Loading latest Briefings for the user
    function loadlatest(){

          $this->load->model('Briefingmodel','bmodel');
          $result = $this->bmodel->get_first_three_briefings($this->session->userdata('userid') );
           echo json_encode($result);


    }
    //Loading briefing for user by Privacy level (e.g. Group, Private, Public)
    function loadbyprivacy(){

            $privacy = $this->uri->segment(3);
            if (isset($privacy)){
              $this->load->model('Briefingmodel','bmodel');
              $result = $this->bmodel->get_byprivacy($this->session->userdata('userid'),$privacy);

               echo json_encode($result);

            }
    }

    //Load all briefings for a user
    function loadall(){
         $this->load->model('Briefingmodel','bmodel');
          $result = $this->bmodel->get_all_briefings($this->session->userdata('userid'));
                echo json_encode($result);
    }

    //Displaying a single briefing view
    function sview(){
   
            
                     
        $br_id = $this->uri->segment(3);
        if (isset($br_id) && $br_id != null && is_numeric($br_id)){
            
              //check if user is logged in
              $this->load->model('loginmodel');
              $isloggedin = false;
              
              if ($this->loginmodel->isLoggedin()){
                $isloggedin = true;  
              }
            
            $this->load->model('Briefingmodel','bmodel');   
            $this->lang->load('briefing','english'); 
              $data = array(
                'isloggedin' => $isloggedin,            
                'result' => $this->bmodel->findById($br_id)
                ); 
                
            $this->load->view("viewbriefing",$data);

        }
    }
   

    //Send Quick briefing
      function sendquick(){
            
         $mysqldate = date( 'Y-m-d H:i:s', time() );
         $phpdate = strtotime( $mysqldate );
            
          //expect an array of ids
          $arr_ids = explode(',',$this->input->post('ids'));
           $sendername = getFullName();
           $recipients = "";   
              
      // Id of a briefing
           $briefingid = $this->input->post('bid');
           
           if (is_numeric($briefingid) ){
               
               //load data about briefing
               $this->load->model('Briefingmodel','bmodel'); 
                             
               $row = $this->bmodel->findById($briefingid);
                if ($row == null){
                    return;
                }
               $items = array();
               
               $i = 0;
                 foreach ( split('<sp>',$row->body) as $item){
                          if ($item != ''){
                              $items[$i] = array('section'=>$item);
                              $i++;
                          }
                        
                    }
                
               
               
               //parse and load template to send
               $this->load->library('parser');

               $data = array(
                'title' => $row->title,            
                'url' => $this->config->item('base_url').'briefing/sview/'.$briefingid,
                'items' => $items,
                'rss_url'=>  $this->config->item('base_url').'grouptalk/feeds/public/'.$this->encrypt->encode($this->session->userdata('userid'))
                );

            $body = $this->parser->parse('briefing_template', $data, TRUE);
            
            //Load the recipients and their names   
               $this->load->model('Contactmodel','cmodel');
               $result = $this->cmodel->getContactsByIds($arr_ids,$this->session->userdata('userid'));  
                 
                 
               
                  
                foreach ($result as $rowx)
                {                    
                   $recipients .= $rowx->email .',';
                }
                  
                
               
                  $recipients = rtrim($recipients,',');
                   
                   
                 $this->lang->load('briefing','english'); 
              
              
              
                 if ($this->sendNoticeViaMail($sendername,$row->title,$recipients,$body) == true){
                     
                      echo "OK\n".$this->lang->line("briefing_sent");
                      
                       //On success update Last Sent field 
                      $this->bmodel->update_lastsent($briefingid,$mysqldate);
                      
                      //AUDIT
                       $this->load->model('Historymodel','hmodel');    
                       $this->hmodel->create($briefingid,$this->session->userdata('userid'),$mysqldate);
                      
                 }else{
                    echo "FAIL\n".$this->lang->line("briefing_unsent"); 
                 }
              
           }
      }

    function sendNoticeViaMail($sendername,$title,  $recipients, $body){

       if (isset($sendername)){

        $this->load->library('email');
        
        
        $this->email->from('noreply@getbriefed.net', 'Briefing on behalf of '.$sendername);
        $this->email->to($recipients);
         $this->email->set_mailtype('html');
         
        $this->email->subject('Briefing on: '.$title);
        $this->email->message($body);
        $this->email->set_alt_message('This is the alternative message'); // NO html version

        if (!$this->email->send()){
            //echo $this->email->print_debugger();
            return false;
        }else{
            return true;
        }
        

       }

    } //END function send Notice ViaMail
    
    
    function feed(){
         $encodedid = $this->uri->segment(3);
         //usually it would be 
         
         
         //get brief notes for the group or 
    }


   }
?>
