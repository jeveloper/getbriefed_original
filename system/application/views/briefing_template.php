<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>{title}</title>
</head>
<body>

 <p>
     This quick briefing was sent to you via an Online Tool at GetBriefed.net<br/>
     Keep yourself briefed from this person using RSS <a href="{rss_url}">Public Feeds</a>
 </p>

<h3>{title}</h3>

<ul>
{items}
<li>{section}</li>
{/items}
</ul>

<p>To view this briefing online visit <a href="{url}">This Link</a></p>
</body>
</html>