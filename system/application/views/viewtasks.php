<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for Windows (vers 14 February 2006), see www.w3.org">

  <title><?= $title;?></title>
  <script src="../javascript/prototype.js" type="text/javascript">
</script>
  <script src="../javascript/effects.js" type="text/javascript">
</script>
  <script src="../javascript/dragdrop.js" type="text/javascript">
</script>
  <script src="../javascript/controls.js" type="text/javascript">
</script>
  <script src="../javascript/starRank.js" type="text/javascript">
</script>
  <style type="text/css">
   
  body {

  background-color: #fff;
  margin: 40px;
  font-family: Lucida Grande, Verdana, Sans-serif;
  font-size: 14px;
  color: #4F5155;
  }

  a {
  color: #003399;
  background-color: transparent;
  font-weight: normal;
  }

  h1 {
  color: #444;
  background-color: transparent;
  border-bottom: 1px solid #D0D0D0;
  font-size: 16px;
  font-weight: bold;
  margin: 24px 0 2px 0;
  padding: 5px 0 6px 0;
  }

  code {
  font-family: Monaco, Verdana, Sans-serif;
  font-size: 12px;
  background-color: #f9f9f9;
  border: 1px solid #D0D0D0;
  color: #002166;
  display: block;
  margin: 14px 0 14px 0;
  padding: 12px 10px 12px 10px;
  }


  p {
  margin:1em;
  line-height:160%;
  }

  .comment {
  border:1px solid;
  font-size:12px;
  line-height:150%;
  margin:10px 10px 0px 20px;
  padding:1em;
  }
  .comment:hover {
  border:1px solid;
  font-size:22px;   
  }
  .commentFooter {
  font-size:11px;
  }             
  .commentIcon {
  float:left;
  background:transparent;
  border:0px none;
  display:inline;
  margin:0;
  margin-top:10px;
  text-align:left;
  }

  .gravatar {
  float:right;
  background:#fcfcfc;
  border:1px solid #ccc;
  padding:2px;
  margin:-18px -18px 4px 4px;
  }



  a, a:link {
  border-color:#9bc30d;
  color:#75930a;
  }
  a:visited{
  border-color:#ddd;
  color:#9bc30d;
  }
  a:hover{
  border-color:#627b08;
  color:#b6b219;
  }
       
       
  #star ul.star { LIST-STYLE: none; MARGIN: 0; PADDING: 0; WIDTH: 85px; HEIGHT: 20px; LEFT: 10px; TOP: -5px; POSITION: relative; FLOAT: left; BACKGROUND: url('http://localhost/notepad/img/stars.gif') repeat-x; CURSOR: pointer; }
  #star li { PADDING: 0; MARGIN: 0; FLOAT: left; DISPLAY: block; WIDTH: 85px; HEIGHT: 20px; TEXT-DECORATION: none; text-indent: -9000px; Z-INDEX: 20; POSITION: absolute; PADDING: 0; }
  #star li.curr { BACKGROUND: url('http://localhost/notepad/img/stars.gif') left 25px; FONT-SIZE: 1px; }
  #star div.user { LEFT: 15px; POSITION: relative; FLOAT: left; FONT-SIZE: 13px; FONT-FAMILY: Arial; COLOR: #888; }
       
       
  #content .important {
   background: #FBE6F2;
   border: 1px solid #D893A1;
   color: #333;
   margin: 10px 0 5px 0;
   padding: 10px;
  }
        
  #content .important p {
   margin: 6px 0 8px 0;
   padding: 0;
  }

  #content .important div {
   margin: 6px 0 8px 0;
   padding: 0;
  }
             
  </style>
  <script type="text/javascript">

  function onReceive(request){
   
    if (request.responseText == 'SUCCESS')
      XNotify('Thanks',true);
    else
      XNotify('Failed to proceed.',false);
      
      
  }
  function onReceive(request,divId){
   
    if (request.responseText == 'SUCCESS'){
      XNotify('Thanks',true);
      
      Element.hide(divId);
      
    }else{
      XNotify('Failed to proceed.',false);
    }
      
      
  }


  function XNotify(message,isSuccess){

    
    var bgcol = '#dfd'; //normal colour
    if (isSuccess == false)
           bgcol = '#FFC6B3';
           
      var notice = $('notification');      
      notice.update(message).setStyle({ background: bgcol });
      new Effect.Highlight('notification');  
  }
  function onVote(e,o){
    //original , uses calss star
      //onmousedown="star.update(event,this)"
              
      var n=star.num, v=parseInt($('starUser'+n).innerHTML);
       n=o.id.substr(4); $('starCur'+n).title=v;
        
        XNotify('Thanks for voting, your vote:'+v,true);
      
       //Disable voting: 
       //var str = $('star2');
       //str.update().setStyle({display:'none'});
       
       
  }



  </script>
</head>

<body>
  <div id="content">
    <h1><?= $heading;?></h1>

    <div class="important" id="notification">
      These functions are from JavaScriptMacro class which was
      merged into Scriptalaculous.
    </div>

    <div id="star">
      <ul id="star2" class="star" onmousedown="onVote(event,this)"
      onmousemove="star.mouse(event,this)" title="Rate This!">
        <li id="starCur2" class="curr" title="83" style=
        "width: 83px;"></li>
      </ul>

      <div id="starUser2" class="user">
        83%
      </div><br style="clear: both;">
    </div><code>system/application/controllers/welcome.php</code>

    <div class="comment" id="comment-140283">
      <div class="gravatar">
        <img src="../img/avatar1.png" alt="Serge's Gravatar">
      </div>

      <p>Oops, forgot about your plugin heh. Was trying to install
      as few plugins as possible though and it�s my preference to
      mess with views than with controllers for this little project
      at work I am working on at the moment.</p>

      <p>But definitely would use KRJS at some point! Thanks!</p>

      <p class="commentFooter">Posted by: <cite><a href=
      'http://blog.codefront.net/' rel='external nofollow'>Chu
      Yeow</a></cite> on March 26, 2007 10am</p>
    </div><br>
    <?
  echo $this->ajax->link_to_remote("Login", array('url'=> 'http://localhost/notepad/index.php/login','complete'=> 'onReceive(request)' ));
?>


    <div id="frmAdd">
      <? echo $this->ajax->form_remote_tag( array('url'=>'http://localhost/notepad/index.php/login/addForm', 'complete'=>'onReceive(request,\'frmAdd\')') ); ?>


      <p><label for="author">Name</label> (required)<br>
      <input type="text" name="author" id="author" class="textarea"
      value="" size="28" tabindex="1"></p>

      <p><label for="email">E-mail</label> (required, but never
      displayed)<br>
      <input type="text" name="email" id="email" value="" size="28"
      tabindex="2"></p>

      <p><label for="url">Website</label><br>
      <input type="text" name="url" id="url" value="" size="28"
      tabindex="3"></p>

      <p><input name="submit" id="submit" type="submit" tabindex=
      "6" value=
      "Submit Comment"></p><? echo "</form>" ?>

    </div>
  </div>
</body>
</html>
