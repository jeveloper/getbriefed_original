<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - Learn</title>

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/welcome.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_buttons.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' /> 

<meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>
</head>

<body>

   <div id="header">
        <p><?= anchor("/login","Login"); ?> | <?= anchor("/registration","Create Free Account", array("class"=>"asterisk")); ?>
        </p>
    </div>



  <div id="content_main">

    <div id="header_large">
                <!-- header large section -->
                 <div id="header_left"></div>
                 <div id="header_text">
                  <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>Watch and Learn (screencast series)</p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
                 </div> 
    </div>
      

    <div id="sidebar">
       <div id="container_green">

            <div class="top" >
            <h3>Navigation</h3>  
             <p><?= anchor("","Home"); ?><br/>
             <?= anchor("/login","Login"); ?><br/>
              <?= anchor("/registration","Create Free Account"); ?>
             <p>
            </div>

            <div class="footer"></div>

        </div>


     

    </div>


    <div id="center_section">

          
           <div class="lightblue_rbroundbox">
                    <div class="lightblue_rbtop"><div></div></div>
                        <div class="rbcontent">
                          <!-- 406x360 Original: containerwidth=1077&containerheight=635& -->
  
                
<object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" width="406" height="360">
<param name="movie" value="http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/jingswfplayer.swf"></param><param name="quality" value="high"></param><param name="bgcolor" value="#FFFFFF"></param>
<param name="flashVars" value="containerwidth=650&containerheight=600&thumb=http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/FirstFrame.jpg&loaderstyle=jing&content=http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/00000002.swf&blurover=false"></param>
<param name="allowFullScreen" value="true"></param><param name="scale" value="showall"></param><param name="allowScriptAccess" value="always"></param>
<param name="base" value="http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/"></param><embed src="http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/jingswfplayer.swf" quality="high" bgcolor="#FFFFFF" width="406" height="360" type="application/x-shockwave-flash" allowScriptAccess="always" flashVars="containerwidth=406&containerheight=360&thumb=http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/FirstFrame.jpg&loaderstyle=jing&content=http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/00000002.swf&blurover=false" allowFullScreen="true" base="http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/" scale="showall"></embed> </object>       
                       
                       
                       <h3><a href="http://screencast.com/t/WXeSToKgkpj" target="_new">Watch it Full Screen</a></h3>
                    </div><!-- /rbcontent -->
                    <div class="lightblue_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
          


                  <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>More screencasts will be created.</p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->



    </div>
<div class="clear"></div>


  </div>



<div id="superfooter">
    <p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>
    </p>

</div>

</body>
</html>
