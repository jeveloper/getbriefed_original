<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - Contact</title>

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/welcome.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' /> 


<meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>
                                          

</head>

<body>

   <div id="header">
        <p><?= anchor("/login","Login"); ?> | <?= anchor("/registration","Create Free Account", array("class"=>"asterisk")); ?>
        </p>
    </div>



  <div id="content_main">

    <div id="header_large">
                <!-- header large section -->
                 <div id="header_left"></div>
                 <div id="header_text">
                  <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>About Us</p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
                 </div> 
    </div>
      

    <div id="sidebar">
       <div id="container_green">

            <div class="top" >
            <h3>Navigation</h3>  
             <p><?= anchor("","Home"); ?><br/>
             <?= anchor("/login","Login"); ?><br/>
              <?= anchor("/registration","Create Free Account"); ?>
             <p>
            </div>

            <div class="footer"></div>

        </div>


     

    </div>


    <div id="center_section">

          
         <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>We are growing and adding new features each day to make this service exciting and useful.<br/>
                           Please send your comments and suggestions to: <br/><strong>singens.inc AT gmail.com<strong>
                          
                        </p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->


                 



    </div>
<div class="clear"></div>


  </div>



<div id="superfooter">
    <p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>
    </p>

</div>

</body>
</html>
