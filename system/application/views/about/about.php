<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - About</title>

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/welcome.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_buttons.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' /> 
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/ticker.css' /> 

<meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>

   
<script src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.0.3/prototype.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.1/scriptaculous.js?load=effects" type="text/javascript"></script>
 <script src="<?= base_url();?>javascript/ticker.js" type="text/javascript"></script> 

</head>

<body>

   <div id="header">
        <p><?= anchor("/login","Login"); ?> | <?= anchor("/registration","Create Free Account", array("class"=>"asterisk")); ?>
        </p>
    </div>



  <div id="content_main">

    <div id="header_large">
                <!-- header large section -->
                 <div id="header_left"></div>
                 <div id="header_text">
                  <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>About this service</p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
                 </div> 
    </div>
      

    <div id="sidebar">
       <div id="container_green">

            <div class="top" >
            <h3>Navigation</h3>  
             <p><?= anchor("","Home"); ?><br/>
             <?= anchor("/welcome/changes","Changes"); ?><br/>    
             <?= anchor("/login","Login"); ?><br/>
              <?= anchor("/registration","Create Free Account"); ?><br/>
               <?= anchor("/welcome/contact","Contact Us"); ?>
             <p>
            </div>

            <div class="footer"></div>

        </div>


     

    </div>


    <div id="center_section">

          
           <div class="lightblue_rbroundbox">
                    <div class="lightblue_rbtop"><div></div></div>
                        <div class="rbcontent">
                       
                <h3>What is it?</h3>
                <p>

                    <ul>
                    <li>Definition of the word Briefing: "is the oral or written disclosure, before the event, of information or instructions concerning an operation, project, or visit."</li>
                    <li>This project is about reaching new heights, new ways of keeping team mates with updated information</li>
                    
                    <li> Our objective is to deliver an effecient tool for a quick brief note to be created and sent or kept FYI</li>
                    </ul>
                </p>

                <h3>How to use it?</h3>
                <p>
                   Create an account or play with our Demo account. Each briefing should have one or more Briefing points, simple point form lines you would add with an Enter or by clicking Checkmark.
                </p>

                 <h3>Why join?</h3>
                <p>
                  We constantly evolve and add new features. We are extremly dynamic to find the optimal way of collaboration.
                  This project will always stay free , for those of you worried. 
                </p>

                    </div><!-- /rbcontent -->
                    <div class="lightblue_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
          


                  <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>In the nearest future we will create screencast to learn this Web 2.0 Service. Though savvy users might simply jump in and start creating brief notes (aka Briefings).
                        <br/>
                        
                          <strong><?= anchor("/welcome/learn","Watch the screencast on How to use GetBriefed" ); ?></strong>
                              <br/>
                              <br/>
                           <div id="newsticker">
                                 <ul>
                                <li>2 October - Sending Briefings is complete</li>
                                <li>20 August - Contacts section is added</li>
                                <li>6 August - Remember Me feature added</li>
                                <li>13 October - New content available</li>
                                <li>15 October - Added screencast tutorial</li>
                                <li>19 October - Adding section titles, updating languages</li>
                                <li>20 October - Briefing management faster and effective</li>
                                <li>26 October - System upgrade, now we're even more faster</li>
                                <li>22 November - GetBriefed now in Beta 2</li>
                                <li>23 November - Multilingual support added</li> 
                            </ul>
                        </div>

                          
                        </p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->

                
                
                  <div class="lightblue_rbroundbox">
                    <div class="lightblue_rbtop"><div></div></div>
                        <div class="rbcontent">
                       
                        <h3>Watch and Learn</h3>
                        
                        <img height="42" width="42" src="http://content.screencast.com/users/sergeBor/folders/Default/media/825f1ae1-1be9-4cc5-bb92-35eb28a2640d/FirstFrame.jpg" border="0"/>
                         <h3><?= anchor("/welcome/learn","Watch the screencast on How to use GetBriefed" ); ?></h3> 
                    </div><!-- /rbcontent -->
                    <div class="lightblue_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
                


    </div>
<div class="clear"></div>


  </div>



<div id="superfooter">
    <p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>
    </p>

</div>

</body>
</html>
