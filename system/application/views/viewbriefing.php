<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed ( <?= $this->lang->line('lbl_preview');?>  <?= $result->title; ?>)- point form briefing made easier </title>
    <?php
  echo $this->load->view('shared/common');
?>


  <script src="<?= base_url();?>javascript/tooltip.js" type="text/javascript"></script>

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/dsx.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/preview.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tooltip.css' />

  <style>
  div.inner {
    width: 960px;
    margin: 0 auto;
}

div.hd {
    padding: 30px 0 0 0;
    background: #272727 url('./images/hd_bg.png') left bottom repeat-x;
}

div.bd {
    float: left;
    width: 600px;
    padding: 0 20px 0 0;
    border-right: 1px solid #CCC;
    margin: 20px 0 20px 0;
}

div.sd {
    padding: 0 0 0 20px;
    margin: 20px 0 20px 0;
    float: right;
    width: 319px;
}

div.ft {
    clear: both;
    border-top: 1px solid #000;
    padding: 20px;
    background: #272727 url('./images/hd_bg.png') left bottom repeat-x;
    color: #FFF;
}

div.ft a {
    color: #FFFF99;
}

div.ft div.inner {
    width: 960px;
    margin: 0 auto;
}

div.ft div.col {
    float: left;
    width: 280px;
    padding-right: 20px;
    margin-right: 20px;
    border-right: 1px solid #666;
}

div.ft h2 {
    margin-bottom: 20px;
}

div.ft p {
    margin: 0 0 10px 0;
    line-height: 18px;
}

div.ft div.col.last {
    margin: 0 !important;
    padding: 0 !important;
    border: 0 !important;
}

div.ft ul {
    margin-left: 10px;
}
div.ft ul li {
    margin: 7px 0;
}
div.ft ul, div.ft li {
    list-style-type: disc;
}

  </style>
</head>

<body  >

<div id="tooltip2" onMouseOver="keeptipopen();" onmouseout="exittoolarea(500);"></div>
<div id="container">

       <div id="header">
       <p>
       <? if ($isloggedin): ?>
              
            Briefing | <?= anchor("/briefing","Dashboard"); ?>| <?= anchor("/subscriber","My Contacts", array("class"=>"cog")); ?>| <?= anchor("/login/logoff","Logoff"); ?>
            
            <? else: ?>
            <?= anchor("/login","Login"); ?> | <?= anchor("/registration","Create Free Account", array("class"=>"asterisk")); ?>
            <? endif; ?>
            </p>
        </div>


        <div id="header_large">
                <!-- header large section -->
                <div id="header_left">
                </div>
          </div>

          <div id="top_section">

               <a href="#" onClick=""></a>


          </div>

      <div id="content_main">

         <!-- THIS IS A PREVIEW  -->
         <div id="briefing" >

              <div id="gtext"  >
                <?= $result->title; ?>
              </div>

              <p>
                  <ul class="gtext_preview_list" >
                  <?
                    foreach ( split('<sp>',$result->body) as $item){
                        echo  ( ($item != '') ?  '<li>'.$item.'</li>' : '');
                    }
                  ?>

                   </ul>

              </p>

              <p class="commentFooter">
                <cite>Posted on:<?=  $result->datecreated; ?></cite>
              </p>


        </div>

      </div> <!-- END OF CONTENT MAIN -->


      <div id="clear"></div>





</div> <!-- END OF CONTAINER -->
<!--
<div id="superfooter"><p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>     </p></div>
    -->
    
    <div class="ft">
    <div class="inner">
        <div class="col">
            <h2>Boring Stuff</h2>

            <p>This is a Brief Note, share it</p>
        </div>
        
        <div class="col">
            <h2>Keep Subscribed</h2>

            <p>Theres lots of ways for you to keep up with me on the web.</p>
            <ul>
                <li>Subscribe to my <a href="http://jeveloper.com/contact">Main Site</a></li>
                <li><a target="_blank" href="http://twitter.com/tennistweet/">Follow me on Twitter</a></li>
            </ul>
        </div>
        
        <div class="col last">

            <h2>Please Note</h2>
            <p>This is a BETA site, Getbriefed.com will be coming</p>
        </div>
        <div class="clear"></div>
    </div>
</div>

</body>
</html>
