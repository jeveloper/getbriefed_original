<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - point form briefing made easier </title>

          <?php
  echo $this->load->view('shared/common');
?>
 <script src="<?= base_url();?>javascript/stringbuilder.js" type="text/javascript"></script>
<script src="<?= base_url();?>javascript/global.js" type="text/javascript"></script>  

  <script src="<?= base_url();?>javascript/protoplugin.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/tooltip.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/dsx.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tooltip.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' />

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tabs.css' /> 

 <script type="text/javascript" language="javascript">
  
  var NO_RECORDS =  '<?= $this->lang->line('no_records');?>';
  
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function unloadx(path,s_url,s_observeid){
 
   
     unloadContacts(path,s_observeid,s_url,NO_RECORDS);
     /*
     
     this doesnt work all the time for some reason
     
       document.observe('scripts:loaded', function(){ unloadContacts(path,s_observeid,s_url,NO_RECORDS) }); 
       
       
     new Ajax.Autocompleter("autocomplete", "autocomplete_choices", \"<?=site_url('subscriber/searchShort');?>\", {
  paramName: "value", 
  minChars: 2, 
  updateElement: addItemToList, 
  indicator: 'indicator1'
});
*/
 $('notification').hide();
 $('add_form').hide();
 $('autocomplete').hide();  

}
function addItemToList(){
alert("hey");
}
</script>
<style>
.selected { background-color: #f1f1f1; }
#suggestionBox { display:none; border: 1px solid silver; }
</style>
</head>
      <script type="text/javascript">
var uservoiceOptions = {
  /* required */
  key: 'getbriefed',
  host: 'getbriefed.uservoice.com', 
  forum: '43265',
  showTab: true,  
  /* optional */
  alignment: 'left',
  background_color:'#f00', 
  text_color: 'white',
  hover_color: '#06C',
  lang: 'en'
};

function _loadUserVoice() {
  var s = document.createElement('script');
  s.setAttribute('type', 'text/javascript');
  s.setAttribute('src', ("https:" == document.location.protocol ? "https://" : "http://") + "cdn.uservoice.com/javascripts/widgets/tab.js");
  document.getElementsByTagName('head')[0].appendChild(s);
}

/*
addLoadEvent(unloadx('<?= base_url();?>','subscriber/getContacts','content_main'));
addLoadEvent(_loadSuper());

addLoadEvent(_loadUserVoice());  
  */
 // _loadSuper = window.onload;
//window.onload = (typeof window.onload != 'function') ? _loadUserVoice : function() { _loadSuper(); _loadUserVoice(); alert("its ok");  };
//unloadx('<?= base_url();?>','subscriber/getContacts','content_main');
    
    Event.observe(window, 'load', function() { unloadx('<?= base_url();?>','subscriber/getContacts','content_main') });
    Event.observe(window, 'load', function() { _loadSuper });    
</script>
<body  >
<div id="tooltip2" onMouseOver="keeptipopen();" onmouseout="exittoolarea(500);"></div>

<div id="container">

       <div id="header">
            <p>My Contacts | <?= anchor("/briefing","Dashboard"); ?>|<?= anchor("/login/logoff","Logoff"); ?>
            </p>
        </div>


        <div id="header_large">
                <!-- header large section -->
                <div id="header_left">
                </div>
          </div>
          
       


          <div id="top_section">
                <div id="tab_header">
            <ul id="primary">
                <li> <?= anchor("/briefing","Dashboard"); ?></li>                
                <li><span>Subscribers</span> </li>

            </ul>
            </div>

       
               <div class="important" id="notification"></div>

          </div>

      <div id="content_main">


            <div id="center_section">
             <br/> 
               <a href="" onclick="$('add_form').toggle();return false;"> <h3><?= $this->lang->line('add_head');?></h3>     </a>
            <div id="add_form">
               <?= form_open("",array( 'id' => 'add', 'name'=>'add')); ?>

                  <p>
                  <dl> 
                  <dt><label for="t_fname"><?= $this->lang->line('lbl_fname');?>:</label></dt>
                  <dd><input type="text" name="t_fname" id="t_fname" class="jsrequired" alt=" "
                  value="" size="28"  tabindex="1"/></dd>

                   
                  <dt><label for="t_lname"><?= $this->lang->line('lbl_lname');?>:</label></dt>
                   <dd><input type="text" name="t_lname" id="t_lname"  alt="Last name"
                  value="" size="28"  tabindex="2"/></dd>
                   
                  <dt><label for="t_email"><?= $this->lang->line('lbl_email');?>:</label></dt>
                   <dd><input type="text" name="t_email" id="t_email" class="jsrequired jsvalidate_email" alt=" "
                  value="" size="28"  
                  tabindex="3" onKeyDown="if(event.keyCode==13){ addContact('<?=site_url('subscriber/add');?>');return false;}"/></dd>

                  </dl> 

                  <a href="#"   tabindex="4" onclick="addContact('<?=site_url('subscriber/add');?>');return false;"><img src="<?= base_url();?>img/toolicons/24-member-add.png" border="0"  alt="Add"/></a>
                   <a href="#" id="question" onmouseover="tooltip(this,'<h3>Add Your Contacts</h3><p>Adding contacts allows you to pick a person(s) to brief.</p>');" onclick="return false;" onmouseout="exittoolarea(1250);" ></a>
                 

                  <div id="msg"></div>
                  </p>
                  <?=  form_close();?>
            </div>
            
                  <br/>   
                 <h3><?= $this->lang->line('list_head');?></h3>             
                <div class="subsection" >
                  <div id="lst"></div>
                </div>
                
           <input type="text" id="autocomplete" name="autocomplete_parameter"/>
<span id="indicator1" style="display: none">
 Working...
</span>
<div id="autocomplete_choices" class="autocomplete"></div>

                        

            </div>
            <!--  END OF CENTER SECTION -->


      </div> <!-- END OF CONTENT MAIN -->


      <div id="clear"></div>





</div> <!-- END OF CONTAINER -->
<div id="superfooter"><p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>     </p></div>
</body>
</html>
