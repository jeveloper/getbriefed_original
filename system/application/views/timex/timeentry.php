<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - point form briefing made easier </title>


<script src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.0.3/prototype.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.1/scriptaculous.js?load=effects,controls" type="text/javascript"></script>
 


  <script src="<?= base_url();?>javascript/protoplugin.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/global.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/stringbuilder.js" type="text/javascript"></script>  
  
  
  <script src="<?= base_url();?>javascript/tooltip.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/dsx.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tooltip.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' />       

 <script type="text/javascript" language="javascript">
  
  
function unloadx(path,s_url,s_observeid){
   Prototype.include([path+'javascript/timetracker.js']);

    document.observe('scripts:loaded', function(){ unloadHours(path,s_observeid,s_url,'<?= $this->lang->line('no_records');?>') });
     $('notification').hide();                       

}

</script>

</head>

<body onload="unloadx('<?= base_url();?>','hours/gethours/<?=$this->uri->segment(3);?>','content_main');" >
<div id="tooltip2" onMouseOver="keeptipopen();" onmouseout="exittoolarea(500);"></div>

<div id="container">

       <div id="header">
            <p>My Times |  <?= anchor("/project","Projects"); ?> | <?= anchor("/briefing","Dashboard"); ?>|<?= anchor("/login/logoff","Logoff"); ?>
            </p>
        </div>


        <div id="header_large">
                <!-- header large section -->
                <div id="header_left">
                </div>
          </div>

          <div id="top_section">

    
                  
               <div class="important" id="notification"></div>

          </div>

      <div id="content_main">


            <div id="center_section">
                 <h3><?= $this->lang->line('list_head');?></h3>    
                 
            <!--  Headers -->  
                <table border="1" id='lst' >
                
                <thead>
                <tr><th>Date</th><th>Hours</th><th>Description</th><th>Is Billable</th></tr>
                  </thead> 
                  
                <tr id='addForm'>
                 <?= form_open("",array( 'id' => 'add', 'name'=>'add')); ?> 
                        <td>
                            <input type="text" name="t_date" id="t_date" 
                          value="2008-01-01" size="7"  tabindex="1">
                            </td>
                            
                            <td>
                            <input type="text" name="t_hours" id="t_hours" 
                          value="" size="4"  tabindex="2">
                          
                            </td>
                            
                            <td>
                            <input type="text" name="t_desc" id="t_desc" 
                          value="" size="35"  tabindex="3" onkeydown="if(event.keyCode==13){ addHour('<?=site_url('hours/add/'.$this->uri->segment(3));?>');return false;}">
                            </td>
                            
                            <td>
                            <input type="checkbox" name="t_isbill" id="t_isbill" 
                          checked="checked" tabindex="4">
                          <a href="#" onclick="addHour('<?=site_url('hours/add/'.$this->uri->segment(3));?>');return false;">Add</a>
                            </td>
                      <?=  form_close();?>  
                </tr>
                 
                    
              
                
                  </table>
            
          
            </div>
            <!--  END OF CENTER SECTION -->


      </div> <!-- END OF CONTENT MAIN -->


      <div id="clear"></div>





</div> <!-- END OF CONTAINER -->
<div id="superfooter"><p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>     </p></div>
</body>
</html>
