<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - point form briefing made easier </title>


<script src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.0.3/prototype.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.1/scriptaculous.js?load=effects,controls" type="text/javascript"></script>
 


  <script src="<?= base_url();?>javascript/protoplugin.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/global.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/stringbuilder.js" type="text/javascript"></script>  
  
  
  <script src="<?= base_url();?>javascript/tooltip.js" type="text/javascript"></script>
  <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/dsx.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tooltip.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' />       

 <script type="text/javascript" language="javascript">
  
  
function unloadx(path,s_url,s_observeid){
   Prototype.include([path+'javascript/timetracker.js']);

    document.observe('scripts:loaded', function(){ unloadProjects(path,s_observeid,s_url,'<?= $this->lang->line('no_records');?>') });
     $('notification').hide();                       

}

</script>

</head>

<body onload="unloadx('<?= base_url();?>','project/loadMyProjects/full','content_main');" >
<div id="tooltip2" onMouseOver="keeptipopen();" onmouseout="exittoolarea(500);"></div>

<div id="container">

       <div id="header">
            <p>My Projects | <?= anchor("/briefing","Dashboard"); ?>|<?= anchor("/login/logoff","Logoff"); ?>
            </p>
        </div>


        <div id="header_large">
                <!-- header large section -->
                <div id="header_left">
                </div>
          </div>

          <div id="top_section">


          <?= form_open("",array( 'id' => 'add', 'name'=>'add')); ?>

                  <p>
                  <label for="t_name"><?= $this->lang->line('lbl_name');?>:</label> (*)
                  <input type="text" name="t_name" id="t_name" class="jsrequired"   alt="&nbsp;&nbsp;&nbsp;&nbsp;" 
                  value="" size="28"  tabindex="1">


                  <label for="t_fee"><?= $this->lang->line('lbl_fee');?>:</label>
                   <input type="text" name="t_fee" id="t_fee"  class="jsrequired jsvalidate_number" alt="&nbsp;&nbsp;&nbsp;&nbsp;"
                  value="" size="28"  tabindex="2" onKeyDown="if(event.keyCode==13){ addProject('<?=site_url('project/add');?>');return false;}"> 
                    

                  <a href="#"   tabindex="4" onclick="addProject('<?=site_url('project/add');?>');return false;"><img src="<?= base_url();?>img/toolicons/24-em-check.png" border="0"   alt="<?= $this->lang->line('btn_add');?>"/></a>

                  <a href="#" id="question" onmouseover="tooltip(this,'<h3>Add Project</h3><p>Adding separate projects is convenient as you can track hours spent based on project.</p>');" onclick="return false;" onmouseout="exittoolarea(1250);" ></a>

                  <div id="msg"></div>
                  </p>
                  <?=  form_close();?>
                  
               <div class="important" id="notification"></div>

          </div>

      <div id="content_main">


            <div id="center_section">
              <h3><?= $this->lang->line('list_head');?></h3>               
             <!--  List of Projects -->
               <div id="lst"></div>
          
            </div>
            <!--  END OF CENTER SECTION -->


      </div> <!-- END OF CONTENT MAIN -->


      <div id="clear"></div>





</div> <!-- END OF CONTAINER -->
<div id="superfooter"><p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>     </p></div>
</body>
</html>
