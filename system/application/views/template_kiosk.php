<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="ROBOTS" content="NONE" />

    <title>GetBriefed: Login</title>


  <script src="http://www.google.com/jsapi"></script>

  <script>
  google.load("prototype", "1.6",{uncompressed:false});
  google.load("scriptaculous", "1.8.1",{uncompressed:false});

  </script>
            <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/kiosk.css" media="all" />
            <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
</head>
<body>

<div id="top_wrapper"></div>
<div id="wrapper">
        <div id="top">

            <div class="left"><span id="storeName" class="store">GetBriefed:</span><span class="app">Login</span></div>
            <div class="right"><div id="logo"></div></div><div class="clearer"></div>
        </div>

    <div id="main-sidebar">
         <div id="sidebar">
        <div class="title">Login</div>

        <div id="guest-side">
            <p>
            If you haven't an account <?= anchor("/registration","Register"); ?>
            </p>
        </div>
    </div>

     <?= form_open("login/process",array( 'id' => 'login', 'name'=>'login')); ?>
    <div id="content">
            <div id="signin">
                <div class="grayform">
                    <table width="100%" cellpadding="4" cellspacing="0" border="0">


                        <tr>
                            <td class="nopad"><label for="email" accessKey="f">Email:</label><span class="required">*</span></td>
                            <td class="nopad"><label for="password" accessKey="l">Password:</label><span class="required">*</span></td>
                        </tr>
                        <tr>         <!-- signin .text
                        original: class="width250 text"
                        override to: input { border:1px solid #000000; padding:2px; margin-bottom:3px; font-family:"Lucida Sans Unicode", "Trebuchet MS", Tahoma, sans-serif; font-size:11px; }
                        -->
                            <td>

                            <input  maxlength="42"  class="jsrequired jsvalidate_email" alt="Please enter email."  id="email" type="text" name="email" /></td>
                            <td><input maxlength="42" class="jsrequired" alt="Password can't be empty." id="password" type="password" name="password" /></td>

                        </tr>
                        <tr>
                            <td colspan="2"></td>
                        </tr>


                        <tr>
                            <td colspan="2" class="nopad"><?= form_submit("","Login");?>
                            <?= $this->validation->error_string  ; ?>
                            </td>
                        </tr>



                    </table>
                </div>
            </div>
        </div>
        <?=  form_close();?>
    <div class="clearer"></div>

    </div>
    <div id="bottom">
         <!-- SECTION FOR BOTTOM -->
        <div id="navbuttons">
            <div class="left"><a class="back" href="">Back</a></div>
            <div class="right">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td><a class="exit" >Exit</a></td>
                        <td><span id="enablednextbutton"><a onclick="javascript:return validateUserInfo();" class="next" href="#">Next</a></span></td>

                    </tr>
                </table>
            </div>
        </div>
        -->
    </div>

</div>
<div id="bot_wrapper"></div>


</body>

</html>