<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>

    <title>GetBriefed: Login</title>


                          <?php
  echo $this->load->view('shared/common');
?>
            <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/kiosk.css" media="all" />
            <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
</head>
<body>

<div id="top_wrapper"></div>
<div id="wrapper">
        <div id="top">

            <div class="left"><span id="storeName" class="store">GetBriefed:</span><span class="app">Login</span></div>
            <div class="right"><div id="logo"></div></div><div class="clearer"></div>
        </div>

    <div id="main-sidebar">
         <div id="sidebar">
        <div class="title">Login</div>

        <div id="guest-side">
            <p>
            If you haven't an account <?= anchor("/registration","Register"); ?>
            </p>
        </div>
    </div>

     <?= form_open("login/process",array( 'id' => 'login', 'name'=>'login')); ?>
    <div id="content">
            <div id="signin">
                <div class="grayform">
                    <table width="100%" cellpadding="4" cellspacing="0" border="0">


                        <tr>
                            <td class="nopad"><label for="email" accessKey="f">Email:</label></td>
                            <td class="nopad"><label for="password" accessKey="l">Password:</label></td>
                        </tr>
                        <tr>
                            <td>

                            <input  maxlength="42"  class="jsrequired jsvalidate_email" alt="Please enter email."  id="email" type="text" name="email" value="<?=$email;?>"/></td>
                            <td><input maxlength="42" class="jsrequired" alt="Password can't be empty." id="password" type="password" name="password" value="<?=$password;?>"/></td>

                        </tr>
                        <tr>
                            <td colspan="2">Remember me:<input type="checkbox" id="rem" name="rem" value="checked"/></td>
                        </tr>


                        <tr>
                            <td colspan="2" class="nopad"><?= form_submit("","Login");?>
                            <br/><?php echo validation_errors(); ?>
                            </td>
                        </tr>



                    </table>
                </div>
            </div>
        </div>
        <?=  form_close();?>
    <div class="clearer"></div>

    </div>
    <div id="bottom">
         <!-- SECTION FOR BOTTOM -->
        <div id="navbuttons">
 <p>  <?= anchor("","Welcome page" ); ?> | <?= anchor("/registration","Free Account" ); ?>
    </p>
        </div>

    </div>

</div>
<div id="bot_wrapper"></div>

 <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5925990-2");
pageTracker._trackPageview();
</script>
</body>

</html>