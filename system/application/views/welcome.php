<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
<title>GetBriefed - point form briefing made easier</title>

          <?php
  echo $this->load->view('shared/common');
?>

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/welcome.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_buttons.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_boxes.css' />  
<meta name="verify-v1" content="pRySn5coi0OMPXfSaM/T+M2NaTzUFGPvrmYmkVhvY4k=" />
<meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>
</head>
 <script type="text/javascript">
var uservoiceOptions = {
  /* required */
  key: 'getbriefed',
  host: 'getbriefed.uservoice.com', 
  forum: '43265',
  showTab: true,  
  /* optional */
  alignment: 'left',
  background_color:'#f00', 
  text_color: 'white',
  hover_color: '#06C',
  lang: 'en'
};

function _loadUserVoice() {
  var s = document.createElement('script');
  s.setAttribute('type', 'text/javascript');
  s.setAttribute('src', ("https:" == document.location.protocol ? "https://" : "http://") + "cdn.uservoice.com/javascripts/widgets/tab.js");
  document.getElementsByTagName('head')[0].appendChild(s);
}
_loadSuper = window.onload;
window.onload = (typeof window.onload != 'function') ? _loadUserVoice : function() { _loadSuper(); _loadUserVoice(); };
</script>
<body>

   <div id="header">
        <p><?= anchor("/login","Login"); ?> | <?= anchor("/registration","Create Free Account", array("class"=>"asterisk")); ?> | 
     <!-- AddThis Button BEGIN -->
<a href="http://www.addthis.com/bookmark.php?v=250&pub=jeveloper" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s7.addthis.com/static/btn/lg-bookmark-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js?pub=jeveloper"></script>
<!-- AddThis Button END -->


        </p>
    </div>



  <div id="content_main">

    <div id="header_large">
                <!-- header large section -->
                <div id="header_left">
                </div>
    </div>


  <div id="sidebar">
       <div id="container_green">

            <div class="top" >
            <h3>Team Briefing</h3>
             <p>Group briefing, concise and easy.<p>
            </div>

            <div class="footer"></div>

        </div>


           <div id="container_blue">

            <div class="top" >
            <h3>What is it?</h3>
             <p>Quick as a Twitter, practical for group. <p>
            </div>

            <div class="footer"></div>

        </div>

    </div>

    <div id="center_section">

          <div id="container_orange">
               <div class="roundtop">
                 <img src="<?= base_url();?>img/general/tl.gif" alt=""
                 width="15" height="15" class="corner"
                 style="display: none" />
               </div>

               <p>
              Simply start by creating a Brief Note, you'll see.
               
       
               <br/>


               <?= anchor("/registration","<span>Quick Account</span>", array('class'=>'button')); ?> <?= anchor("/login","<span>Login</span>", array('class'=>'button')); ?>

               <br/>

               Try it out with a Demo: <?= anchor("/login/view/demo","<span>Demo Account</span>", array('class'=>'button')); ?>
               
               </p>

               <div class="roundbottom">
                 <img src="<?= base_url();?>img/general/bl.gif" alt=""
                 width="15" height="15" class="corner"
                 style="display: none" />
               </div>

            </div>   <!-- //container_orange -->
            
            
                 <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>
                        <div class="rbcontent">
                        <p>Watch a Demo <?= anchor("/welcome/learn","Screencast"); ?> </p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
                
                      <div class="light_rbroundbox">
                    <div class="light_rbtop"><div></div></div>    
                        <div class="rbcontent">
                        <p>Want to reach us?  <?= anchor("/welcome/contact","Contact Us"); ?> </p>
                    </div><!-- /rbcontent -->
                    <div class="light_rbbot"><div></div></div>
                </div><!-- /rbroundbox -->
            

    </div>
    <div class="clear"></div>


  </div>



<div id="superfooter">
    <p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>

    </p>

</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5925990-2");
pageTracker._trackPageview();
</script>
</body>
</html>
