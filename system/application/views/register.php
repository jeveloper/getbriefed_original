<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="keywords" content="online briefing, briefing, bulletin, information, web, get briefed, briefed"/>
<meta name="description" content="getbriefed is an online project management tool , to brief collegues or others on any topic in point form"/>

    <title>GetBriefed: registration</title>

  <script src="http://www.google.com/jsapi"></script>

           <?php
  echo $this->load->view('shared/common');
?>
  </script>
    <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>
            <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/kiosk.css" media="all" />

</head>

</head>
<body>

<div id="top_wrapper"></div>
<div id="wrapper">
        <div id="top">

            <div class="left"><span id="storeName" class="store">GetBriefed:</span><span class="app">Registration</span></div>
            <div class="right"><div id="logo"></div></div><div class="clearer"></div>
        </div>

    <div id="main-sidebar">
         <div id="sidebar">
        <div class="title">Create an Account</div>

        <div id="guest-side">
            <p>
            Create a GetBriefed account, simplifying project management is the key of this web service.
            If you have an account <?= anchor("/login","Login"); ?>
            <br/>What do you get:
            <ul>
                <li>Free Account</li>
                <li>Ease of use</li>
                <li>Multi lingual support</li>
            </ul>
            </p>
        </div>
    </div>


    <div id="content">
    <?= form_open("registration/process",array( 'id' => 'reg', 'name'=>'reg')); ?>
            <div id="signin">
                <div class="grayform">
                    <table width="100%" cellpadding="4" cellspacing="0" >
                        <tr>
                            <td class="nopad"><label for="fn" accessKey="f">First name:</label></td>
                            <td class="nopad"><label for="ln" accessKey="l">Last name:</label></td>
                        </tr>
                        <tr>
                            <td><input  maxlength="42" class="jsrequired" alt="We'd like to address you by name." id="fn" type="text" name="firstname" value="<?=$fname;?>" /></td>
                            <td><input  maxlength="42" class="jsrequired" alt="Why not." id="ln" type="text" name="lastname" value="<?=$lname;?>"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                        </tr>


                        <tr>
                            <td colspan="2" class="nopad"><label for="email" accessKey="e">email address:</label></td>
                        </tr>
                        <tr>

                            <td colspan="2"><input  maxlength="84" class="jsrequired jsvalidate_email" alt="Please enter an email address." id="email" type="text" name="email" value="<?=$email;?>" /></td>
                        </tr>

                        <tr>
                            <td class="nopad"><label for="pw" accessKey="f">Password:</label></td>
                            <td class="nopad"><label for="pw2" accessKey="l">Password repeat:</label></td>
                        </tr>
                        <tr>
                            <td><input  maxlength="42" class="width250 text" id="pw" type="password" name="password" /></td>
                            <td><input  maxlength="42" class="width250 text" id="pw2" type="password" name="password2" /></td>
                        </tr>



                       <tr>
                            <td  colspan="2"  class="nopad"><label for="pw" accessKey="f">Type the following:</label></td>

                        </tr>
                        <tr>
                            <td><input  maxlength="42" class="width250 text" id="captcha" type="text" name="captcha" /></td>
                            <td><?= $captcha;?></td>
                        </tr>




                          <tr>
                            <td colspan="2"><?= form_submit("","Register");?>
                        <?= $this->validation->error_string  ; ?>
                            </td>

                        </tr>

                    </table>
                </div>
            </div>
        </div>

        <?= form_close(); ?>
    <div class="clearer"></div>

    </div>
    <div id="bottom">
         <!-- SECTION FOR BOTTOM -->
        <div id="navbuttons">
 <p>  <?= anchor("","Welcome page" ); ?> | <?= anchor("/login","Login" ); ?>
    </p>
        </div>

    </div>

</div>
<div id="bot_wrapper"></div>

 <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5925990-2");
pageTracker._trackPageview();
</script>
</body>

</html>