<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">



<html>

<head>

<title>GetBriefed - point form briefing made easier </title>


    <?php
  echo $this->load->view('shared/common');
?>
 <script>
          
          //GLOBAL settings 
          var NO_BRIEFINGS = "<?= $this->lang->line('no_records');?>";
          var NO_CONTACTS = "<?= $this->lang->line('no_records_contacts');?>";                  
 
   //setttings
   var send_briefing_url =  '<?=site_url('briefing/sendquick/');?>';
   var add_briefing_url = '<?=site_url('briefing/add');?>';
   //


  function unloadx(path,s_url,observe_id){ 
    unloadBriefings(path,s_url,observe_id,NO_BRIEFINGS);
     $('notification').hide();    
     $('sidebar').hide();
     toggleDivs(['tags','lbl_tags']);                                                                 
       
}

  </script>



<link rel="stylesheet" href="<?= base_url();?>css/modalbox.css" type="text/css" media="screen" />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/ticker.css' />

<script src="<?= base_url();?>javascript/modalbox.js" type="text/javascript"></script>

<script src="<?= base_url();?>javascript/stringbuilder.js" type="text/javascript"></script>  
<script src="<?= base_url();?>javascript/ticker.js" type="text/javascript"></script> 
<script src="<?= base_url();?>javascript/global.js" type="text/javascript"></script> 


  <script src="<?= base_url();?>javascript/protoplugin.js" type="text/javascript"></script>

  <script src="<?= base_url();?>javascript/jsvalidate_beta05.js" type="text/javascript"></script>

  <script src="<?= base_url();?>javascript/tooltip.js" type="text/javascript"></script>
 



<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/dsx.css' />

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/basic_ground.css' />

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/round_buttons.css' />

<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tooltip.css' />
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/tabs.css' />  
<link rel='stylesheet' type='text/css' media='all' href='<?= base_url();?>css/select_contact.css' />  


</head>

<body onload="unloadx('<?= base_url();?>','briefing/loadlatest','content_main');" >

<div id="tooltip2" onMouseOver="keeptipopen();" onmouseout="exittoolarea(500);"></div>

<div id="container">



       <div id="header">

            <p>Dashboard |  <?= anchor("/subscriber","My Contacts", array("class"=>"cog")); ?>| <?= anchor("/login/logoff","Logoff"); ?>

            </p>

        </div>

        <div id="header_large">

                <!-- header large section -->

                <div id="header_left">

                </div>

          </div>



          <div id="top_section">

            <div id="tab_header">
                <ul id="primary">
                                  
                    <li><span>Dashboard</span> </li>
                    <li> <?= anchor("/subscriber","My Contacts"); ?></li>  
                </ul>
            </div>
              
              <br/> 
               <div id="control_section">
               <a href="#" onClick="toggleDiv('lst','slide');">Show/Hide</a>



              &nbsp;<?= $this->lang->line('lbl_switch');?>: &nbsp;

              <a href="#" onClick="alterType('a'); return false;">a</a>|

              <a href="#" onClick="alterType('A'); return false;">A</a>|

              <a href="#" onClick="alterType('i'); return false;">Roman</a>|

              <a href="#" onClick="alterType(''); return false;">Numeric</a>



               &nbsp; Filter by type:&nbsp;

               <a href="#" onClick="loadupBriefings('<?= base_url();?>','briefing/loadall',NO_BRIEFINGS); return false;"><?= $this->lang->line('lbl_filter_showall');?></a>

               &nbsp;<a href="#" onClick="loadupBriefings('<?= base_url();?>','briefing/loadlatest',NO_BRIEFINGS); return false;"><?= $this->lang->line('lbl_filter_latest');?></a>

               &nbsp;<a href="#" onClick="loadupBriefings('<?= base_url();?>','briefing/loadbyprivacy/private',NO_BRIEFINGS); $('privacy1').selected=true;return false;"><?= $this->lang->line('lbl_privacy_1');?> <img border="0" src="<?= base_url();?>img/toolicons/key.png"/></a>

               &nbsp;<a href="#" onClick="loadupBriefings('<?= base_url();?>','briefing/loadbyprivacy/public',NO_BRIEFINGS);$('privacy2').selected=true; return false;"><?= $this->lang->line('lbl_privacy_2');?> <img border="0" src="<?= base_url();?>img/toolicons/transmit.png"/></a>

               &nbsp;<a href="#"  onClick="loadupBriefings('<?= base_url();?>','briefing/loadbyprivacy/group',NO_BRIEFINGS); $('privacy3').selected=true;return false;"><?= $this->lang->line('lbl_privacy_3');?> <img border="0" src="<?= base_url();?>img/toolicons/group.png"/></a>
               
               &nbsp;<a href="<?= base_url();?>grouptalk/feeds/public/<?=$useridEnc;?>"  > <img border="0" alt="Share public RSS feeds" src="<?= base_url();?>img/bt-rss.gif"/></a>
               </div>
               
               <div class="important" id="notification"></div>
                
          </div>
                              
      <div id="content_main">           
             <br/> <br/> <br/> 
            <div id="center_section"> 
                     
                  <h3><?= $this->lang->line('list_head');?></h3> 
                            
                 <div  id="lst"></div>


                     
                <div class="subsection">
                      <br/><br/>
                        <h3><?= $this->lang->line('tips_head');?></h3>  
                     <div id="newsticker">
                            <ul>
                                <li>While typing a brief line, hit an Enter to add it quicker</li>
                                <li>When adding a contact, hit an Enter on email field</li>
                                <li>Select contacts as you would on IPhone, tap</li>
                                <li>Your brief notes are never trashed</li>
                              
                            </ul>
                        </div>
                            
                   <!-- 
                  <dl class="event">

                    <dt>23 July</dt>

                    <dd>Different styling is applied</dd>

                    <dd>New views are in the works</dd>
                                             
                  </dl>
                        -->
                </div>



            </div>

            <!--  end of center section -->
              <a href=""  onclick="$('sidebar').toggle();return false;"><h3> <?= $this->lang->line('add_briefing_head');?></h3></a>
              
           <div id="sidebar">

               <!-- SIDE BAR FOR various abilities -->

                <?= form_open("",array( 'id' => 'add', 'name'=>'add')); ?>
                
                  <p>                
                      <dl>                         
                        
                          <dt><label for="t_title"><?= $this->lang->line('lbl_title');?>:</label> (*)</dt>

                          <dd><input type="text" name="t_title" id="t_title" class="jsrequired" 
                           alt=" "
                          value="" size="28"  tabindex="1">

                          </dd>



                           <dt><label for="tags" id="lbl_tags"><?= $this->lang->line('lbl_tags');?>:</label>
                           <a href="#" onclick="toggleDivs(['tags','lbl_tags']);">
                          <img border="0" src="<?= base_url();?>img/toolicons/lightbulb.png"/></a>
                           </dt>

                          <dd><input type="text" name="tags" id="tags" 

                          value="" size="28" tabindex="2">

                          </dd>


                                                            
                         <dt><label for="b_point"><?= $this->lang->line('lbl_point');?>:</label> (*)</dt>

                          <dd ><input  type="text" name="b_point" id="b_point" alt=" "  value="" size="28" tabindex="3"  onKeyDown="if(event.keyCode==13){ addBPoint();return false;}">

<a href="#"   onClick="addBPoint();return false;"   tabindex="4"><img src="<?= base_url();?>img/toolicons/24-em-check.png" border="0"  alt="Add to List"/></a>

                          <a href="#" id="question" onmouseover="tooltip(this,'<h3>Add Briefing Points</h3><p>Use the Check button to add as many briefing points as you like, then click Create a Briefing to complete a briefing.</p>');" onclick="return false;" onmouseout="exittoolarea(1250);" ></a>

                          </dd>



                            <dt><label for="privacy"><?= $this->lang->line('lbl_privacy');?>:</label></dt>

                           <dd>

                          <select class="txtfield"  id="privacy" style="width:100px;" name="privacy" >

                          <option id="privacy1" label="<?= $this->lang->line('lbl_privacy_1');?>">private</option>

                          <option id="privacy2" label="<?= $this->lang->line('lbl_privacy_2');?>">public</option>

                          <option id="privacy3" label="<?= $this->lang->line('lbl_privacy_3');?>">group</option>

                          </select>

                          </dd>
             
                      </dl>

                  </p>
      
                  <input type="hidden" id="body" name="body" value=""/>
                
                  <p>
                  <a href="#" class="button"
                  onclick="addBriefing(add_briefing_url);return false;"><span><?= $this->lang->line('add_briefing_btn');?></span></a>
                   
                    <a href="#" class="button"
                  onclick="clearAll();;return false;"><span>Clear</span></a>
                   
                   
                  </p>

                
                  <div id="msg"></div>



                  <?=  form_close();?>

                  <!-- SIDE BAR FOR various abilities END -->



                  <div id="point_list">

                    List (Add by clicking the Plus button):<br/>

                    <ul  id="b_point_list" class="gtext_preview_list" ></ul>

                  <div>



            </div> <!-- END OF SIDEBAR -->

      </div> <!-- END OF CONTENT MAIN -->
   
      <div id="clear"></div>
      
       <!-- First timer  -->
               <div class="container_select" id="first_time" style="display:none;">
                    <h3>First time? Let me help you.</h3>
                    <ul>   
                        <li><a href="#" onclick="$('sidebar').show();Modalbox.hide();return false;">1.Create Brief Note</a></li>      
                        <li><?= anchor("/subscriber","2. Add Contact");?></li> 
                      
                                               
                    </ul>
                    <h3>Still not sure?</h3>
                    <ul>
                      <li><?= anchor("/welcome/learn","Watch a demo"); ?></li> 
                    </ul>
                  <p>
                      <h3><a href="#" onclick="Modalbox.hide(); return false;">Close</a></h3>
                  </p>
     </div>             
       <!-- ftimer END -->
      
      
      <!-- Select Contacts -->
               <div class="container_select" id="select_contacts"  style="display:none;">
                   <?php if ($contacts != null): ?>        
                    <ul>
                        <?php foreach ($contacts as $row): ?>

                        <li  id="<?= $row->id;?>">

                        <a href="#" onclick="VisualSelect('<?= $row->id;?>')"><?= $row->firstname;?> , <?= $row->lastname;?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif;?> 

                   
                   <?php if ($contacts == null): ?>
                        <p><?= $this->lang->line('no_records_contacts');?></p>
                    <?php else:?>
                    
                      <p><a href="#"  class="button" id='send_btn'
                  onclick="sendQuickBriefing(send_briefing_url,'send_btn','send_status');return false;">
                  <span id="send_status"><?= $this->lang->line('send_btn_contact');?></span></a> &nbsp;
                  
                    <?php endif;?>
                    <!-- briefing id hidden field is required -->
                    <input type="hidden" id="bid_selected" /> 
                   <h3><a href="#" onclick="Modalbox.hide(); return false;">Close</a></h3>
                  </p>


     </div>             
      <!-- CONTACTS END -->

</div> <!-- END OF CONTAINER -->

<div id="superfooter"><p>Copyright Singens Inc.  <?= anchor("http://www.jeveloper.com","Singens Consulting" ); ?>     </p></div>

</body>

</html>