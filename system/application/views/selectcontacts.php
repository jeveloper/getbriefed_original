<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<style>

       /*
       
        *{
    margin:0;
    padding:0;
}
 body {
    margin:30px 0 30px 0;
    text-align:center;
    background-color:#62E122;
    font-size:80%;
    font-family:"Lucida Grande", Tahoma, sans-serif;
}

*/

       
#container_select{
    margin:0 auto;
    width:300px;
    padding:40px;
    text-align:left;
    background-color:#fff;
    
     font-size:80%;
    font-family:"Lucida Grande", Tahoma, sans-serif;
}
#container_select ul{
    text-align:center;
    margin:0 0 30px 0;
    list-style:none;
}
#container_select ul a{
    border:1px solid #eee;
    background-color:#f5f5f5;
    color:#444;
    font-size:1.5em;
    line-height:2em;
    padding:5px;
    margin:0 0 20px 0;
    display:block;
}
#container_select ul a:hover{
    border:1px solid #B5DF99;
    background-color:#CDEFB6;
    color:#358610;
}
 #container_select p{
    margin:0 0 1em 0;
}
#container_select a{
    color:green;
}
#container_select a img{
    border:none !important;
}  

</style>

   </head>
   <body>          
    <div id="container_select">

           
                    <ul >
                        <?php foreach ($result as $row): ?>

                        <li  id="<?= $row->id;?>">

                        <a href="#" onclick="VisualSelect('<?= $row->id;?>')"><?= $row->firstname;?> , <?= $row->lastname;?></a></li>
                        <?php endforeach; ?>
                    </ul>


                   
                   <?php if ($result == null): ?>
                        <p><?= $this->lang->line('no_records');?></p>
                    <?php else:?>
                    
                      <p><a href="#" class="button" id='send_btn'
                  onclick="sendQuickBriefing('<?=site_url('briefing/sendquick/'.$briefingid);?>','send_btn','send_status');return false;">
                  <span id="send_status"><?= $this->lang->line('send_btn');?></span></a> &nbsp;
                  
                    <?php endif;?>
                  
                   <a href="#" onclick="Modalbox.hide(); return false;">Close</a>
                  </p>


     </div>             
     </body>
     </html>