<?php
  $lang['error_email_missing'] = "You must submit an email address";
  $lang['add_briefing_head'] = "Create brief note";
  $lang['list_head'] = "Briefing List";
  $lang['news_head'] = "Latest News";
  $lang['tips_head'] = "Recent tips";   
  $lang['add_briefing_btn'] = "Create brief note";
  $lang['created_briefing'] = "A briefing was created successfully.";
  $lang['incomplete_fields'] = "Please fill out the fields.";
  $lang['briefing_removed'] = "A briefing was removed.";
  $lang['briefing_updated'] = "A briefing was updated.";
  $lang['briefing_na'] = "A briefing is not available.";
  $lang['no_records'] = "There are no briefings.";
  $lang['no_records_contacts'] = "There are no contacts in your list.";
  $lang['send_btn_contact'] = "Send Briefing.";
  
  

   $lang['manage_briefings_head'] = "Manage Briefings";


   //Response Messages
   $lang['briefing_sent'] = "Briefing Sent";   
   $lang['briefing_unsent'] = "Briefing could not be sent";
   

   // FORM labels

   $lang['lbl_title'] = "Title (visible to others)";
   $lang['lbl_tags'] = "tags ";
   $lang['lbl_point'] = "Briefing line (add using Checkmark)";
   $lang['lbl_privacy'] = "Privacy";

   $lang['lbl_privacy_1'] = "Private";
   $lang['lbl_privacy_2'] = "Public";
   $lang['lbl_privacy_3'] = "Group";

   $lang['lbl_preview'] = "Debriefing of";

   $lang['lbl_filter_showall'] = "Show All";
   $lang['lbl_filter_latest'] = "Latest";

   $lang['lbl_switch'] = "Switch Styles";

?>
