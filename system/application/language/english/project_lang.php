<?php

//Buttons

$lang['btn_add'] = "Add Project";
$lang['btn_remove'] = "Remove Project";   


//Response messages

$lang['added'] = "Added Project";
$lang['deleted'] = "Project Removed";
$lang['updated'] = "Updated Info.";
$lang['incomplete_form'] = "Fill out the fields correctly.";
$lang['no_records'] = "There are no project in your list.";



//Titles

$lang['page_title'] = "Projects";
$lang['list_head'] = "Project List"; 


//labels

$lang['lbl_name'] = "Name";
$lang['lbl_fee'] = "Fee";


?>
