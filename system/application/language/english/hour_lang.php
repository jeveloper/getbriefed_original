<?php

//Buttons

$lang['add_btn'] = "Add Hour";


//Response messages

$lang['added'] = "Added Hours.";
$lang['deleted'] = "Removed.";
$lang['updated'] = "Updated Info.";
$lang['incomplete_form'] = "Fill out the fields correctly.";
$lang['no_records'] = "There are no records in your list.";



//Titles

$lang['page_title'] = "Hour entries";
$lang['list_head'] = "Hours List"; 

//labels

$lang['lbl_hours'] = "Hours";
$lang['lbl_desc'] = "Description";


?>
