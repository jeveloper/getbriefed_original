<?php
  $lang['request_problem'] = "Your request did not succeed.";   
  $lang['login_btn'] = "Login";
  $lang['logoff_btn'] = "Logoff";
  $lang['register_btn'] = "Register";
  
  
  //Various titles such as Learn, Create free account
   $lang['craccount_title'] = 'Create Free Account';
   $lang['login_title'] = 'Login';
   $lang['contact_title'] = 'Contact Us';
   $lang['about_title'] = 'About';
   $lang['home_title'] = 'Home';
   $lang['dashboard_title'] = 'Dashboard';
   $lang['projects_title'] = 'My Projects';
   $lang['contacts_title'] = 'My Contacts';      
  
?>
