<?php

//Buttons

$lang['add_btn'] = "Add Contact.";
$lang['send_btn'] = "Send Briefing.";         

//Response messages

$lang['added'] = "Added Contact.";
$lang['deleted'] = "Contact Deleted.";
$lang['updated'] = "Updated Contact.";
$lang['incomplete_form'] = "Fill out the fields correctly.";
$lang['no_records'] = "There are no contacts in your list.";



//Titles

$lang['page_title'] = "Subscriber List";
$lang['list_head'] = "Contact List"; 
$lang['add_head'] = "Add Contact"; 

//labels

$lang['lbl_fname'] = "First name";
$lang['lbl_lname'] = "Last name";
$lang['lbl_email'] = "Email";


?>
