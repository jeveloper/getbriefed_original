<?php
  class Briefingmodel extends Model{

      var $id = 0;
      var $tags = '';
      var $body = '';
      var $creatorid = 0;

      var $title = '';
      var $status = ''; //open/closed/deleted
      var $privacy = '';  //private, public , group



      //METHOD CHAIN    PHP5 only
      //$this->db->select('title')->from('mytable')->where('id', $id)->limit(10, 20);
      
      //select takes 2 parameters , first sql , second TRUE OR FALSe, true by default, if you have your own quotes in statements , set FALSE

      function Briefingmodel(){
         parent::Model();
      }

      //Full datetime: 2009-01-08T08:45:00Z+0100
      //Lets use client's timezone
      //+0100 is the timezone adjustment
      
      //CONVERT_TZ(datecreated,'+00:00','+00:00')
      //each usersession contains an adjustment , second parameter 
      
      function getAll(){
        return "title,tags, DATE_FORMAT(datecreated, '%e %b, %Y') as dcreated, CONVERT_TZ(datecreated,'+00:00','-00:00') as datecreated,id,body, creatorid, status,privacy,title, CONVERT_TZ(lastsent,'+00:00','+00:00') as lastsent";

      }
      
      //returns a ROW object accessible via column names
      function findById($id){
         $query = $this->db->get_where('briefing', array('id' => $id));      
         return $query->row();
      }

    // quries for active briefings

    function get_first_three_briefings($id)
    {
        $this->db->select($this->getAll(),false)->from('briefing')->where(array('creatorid'=> $id,'status' =>'open') )->order_by("`datecreated`", 'DESC')->limit(3);
        
        return $this->db->get()->result();
        
       
    }
    
    function get_all_briefings($id)
    {
        $this->db->select($this->getAll(),false)->from('briefing')->where(array('creatorid'=> $id,'status' =>'open') )->order_by('datecreated desc');
         
        return $this->db->get()->result();
    }

      function get_byprivacy($id,$privacy)
    {
        $this->db->select($this->getAll(),false)->from('briefing')->where(array('creatorid'=> $id,'status' =>'open','privacy' =>$privacy) )->order_by('datecreated desc');
        return $this->db->get()->result();
    }

 

    function get_closed_briefings($id)
    {
        $this->db->select($this->getAll(),false)->from('briefing')->where( array('creatorid'=> $id,'status' =>'closed') )->order_by('datecreated desc');
        return $this->db->get()->result();
    }



    function insert_entry($tags,$title, $body,$creatorid, $privacy)
    {
        $this->tags = $tags;
        $this->body = $body;
        $this->creatorid = $creatorid;
        $this->title = $title;
        $this->status = 'open';
        $this->privacy = $privacy;

        $this->db->insert('briefing', $this);
    }


    function close_entry($id){
       $data = array(
               'status' => 'closed',

            );

        $this->db->update('briefing', $data, array('id'=> $id));
    }

    //Purge the record
    function delete_entry($id){
        $this->db->delete("briefing",array('id'=> $id));
    }

    function update_entry($id,$tags,$title, $body)
    {   $this->tags = $tags;
        $this->body = $body;
        $this->title = $title;

        $this->db->update('briefing', $this, array('id'=> $id));
    }

    function update_title($id,$title){
         $data = array(
               'title' => $title

            );

        $this->db->update('briefing', $data, array('id'=> $id));
    }

    function update_tags($id,$tags){
         $data = array(
               'tags' => $tags

            );

        $this->db->update('briefing', $data, array('id'=> $id));
    }
    function update_lastsent($id,$timestamp){
         $data = array(
               'lastsent' => $timestamp

            );

        $this->db->update('briefing', $data, array('id'=> $id));
    }

  }
?>
