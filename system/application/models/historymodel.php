<?php
  class Historymodel extends Model {
  
      function Historymodel(){    parent::Model(); }
      
      var $briefing_id = 0;
      var $user_id = 0;
      var $datesent = '';
      
      //field list
        function allFields(){
        return 'briefing_id, user_id, datesent';

      }
      
      function tablename(){
       return 'history';
      }
      
      //Bean functions
      
 
      
      function create($bid,$uid,$datesent){
       
          $this->briefing_id = $bid;
          $this->user_id = $uid;
          $this->datesent = $datesent;
          
        $this->db->insert($this->tablename(), $this);  
      }
      
     
      
       //TODO
      function delete($id){
          $this->db->delete($this->tablename(),array('id'=> $id));      
      }
      function findByID($id){
        $query = $this->db->get_where($this->tablename(), array('id' => $id));      
         return $query->row();
      }
      function findbyBriefing($id){
         $this->db->select($this->allFields(),false)->from($this->tablename())->where( array('briefing_id'=> $id) )->orderby('datesent');
        return $this->db->get()->result();
      }
      
       function findbyUserid($id){
         $this->db->select($this->allFields(),false)->from($this->tablename())->where( array('user_id'=> $id) )->orderby('datesent');
        return $this->db->get()->result();
      }
      
  
  }
?>
