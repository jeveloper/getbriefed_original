<?php
  class Projectmodel extends Model {
  
      function Projectmodel(){    parent::Model(); }
      
      var $id = "0";
      var $name = '';
      var $fee = '0.0';
      var $creatorid = "0";
      
      //field list
        function allFields(){
        return 'id, name,fee';

      }
      
      function tablename(){
      return 'project';
      }
      
      //Bean functions
      
         function updateStatus($id,$status){
       $data = array(
               'status' => $status

            );

        $this->db->update($this->tablename(), $data, array('id'=> $id));
    }
      
      function create($name, $fee,$creatorid){
        $this->name = $name;
        $this->fee = $fee;
        $this->creatorid = $creatorid;
        
        $this->db->insert($this->tablename(), $this);  
      }
      
      function updateFee($id,$fee){
     
          $data = array(
               'fee' => $fee

            );
           $this->db->update($this->tablename(), $data, array('id'=> $id)); 
      }
      
      
      function updateName($id,$name){
     
          $data = array(
               'name' => $name

            );
           $this->db->update($this->tablename(), $data, array('id'=> $id)); 
      }
      
      function delete($id){
          $this->db->delete($this->tablename(),array('id'=> $id));      
      }
      function findByID($id){
        $query = $this->db->get_where($this->tablename(), array('id' => $id));      
         return $query->row();
      }
      function findbyCreator($id){
         $this->db->select($this->allFields())->from($this->tablename())->where( array('creatorid'=> $id) )->orderby('name','desc');
        return $this->db->get()->result();
      }
      
      //Contains total hours and total non billable hours
      function findbyCreatorFull($id){
         $this->db->select($this->allFields().', IFNULL((select sum(hours) from getbriefed_worktimer wr where wr.projectid = getbriefed_project.id and isbill = 1),0) as billhours, IFNULL((select sum(hours) from getbriefed_worktimer wr where wr.projectid = getbriefed_project.id and isbill = 0),0) as nonhours ',FALSE)->from($this->tablename())->where( array('creatorid'=> $id) )->orderby('name','desc');
        return $this->db->get()->result();
      }
                                                   
  
  }
?>
