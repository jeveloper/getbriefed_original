<?php
  class Contactmodel extends Model {

    var $id = 0;

    var $email   = '';

    var $firstname = '';
    var $lastname    = '';
    var $reffriendid = 0;



    function Contactmodel()
    {
        // Call the Model constructor
        parent::Model();
    }

    function fieldsAll(){
        return 'id, email, firstname, lastname,DATE_FORMAT(dateadded, \'%e %b, %Y\') as dateadded';
    }


    // returns inserted id
    function insert($email,$firstname, $lastname,$reffriendid)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->reffriendid = $reffriendid;

        $this->db->insert('contact', $this);

        return $this->db->insert_id();
    }

    function remove($id){
          $this->db->delete("contact",array('id'=> $id));
    }

    function update_user($id,$email,$firstname, $lastname)
    {
       $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;


        $this->db->update('contact', $this, array('id', $id));
    }


     function isEmailUsed($email){
           $this->db->select("id")->from("contact")->where(array('email'=> trim($email)));
           if ($this->db->get()->num_rows() > 0)
                return true;
            else
                return false;
     }

    function getUser($email){


        $this->db->select($this->fieldsAll(),false)->from('contact')->where(array('email'=> $email) );
        return $this->db->get()->result();
    }

    function findByID($id){

        $this->db->select($this->fieldsAll(),false)->from('contact')->where('id', $id);
        return $this->db->get()->result();

    }

    /// Get all contacts for a userid specified
    function getContacts($userid){
         $this->db->select($this->fieldsAll(),false)->from('contact')->where(array('reffriendid'=> $userid) );
        return $this->db->get()->result();
    }
    
     //Get contacts by their Ids
    function getContactsByIds($arr_ids,$userid){
      $this->db->select('email')->from('contact')->where('reffriendid',$userid)->where_in('id', $arr_ids);     
      return $this->db->get()->result();  
    }

}
?>
