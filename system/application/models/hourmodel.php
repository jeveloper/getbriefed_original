<?php
  class Hourmodel extends Model {
  
      function Hourmodel(){    parent::Model(); }
      
      var $id = "0";
      var $projectid = '0';
      var $sdate = '';
      var $hours = 0;
      var $isbill = 1; //is billable
      var $description  = '';
      
      //field list
        function allFields(){
        return 'id, projectid, DATE_FORMAT(sdate, \'%e %b, %Y\') as date, hours, isbill, description';

      }
      
      function tablename(){
      return 'worktimer';
      }
      
      //Bean functions
      
 
      
      function create($projectid, $sdate, $hours,$isbill,$desc){
       
          $this->projectid = $projectid;
          $this->sdate = $sdate;
          $this->hours = $hours;
          $this->isbill = $isbill;
          $this->description = $desc;
        
        $this->db->insert($this->tablename(), $this);  
      }
      
      function updateHours($id,$hours){
         
         $this->hours = $hours;
           $this->db->update($this->tablename(), $this, array('id'=> $id)); 
      }
      
      function updateDesc($id,$desc){
         
         $this->description = $desc;            
           $this->db->update($this->tablename(), $this, array('id'=> $id)); 
      }
      
      function updateDate($id,$date){
         
         $this->sdate = $date;
           $this->db->update($this->tablename(), $this, array('id'=> $id)); 
      }
      
      
      function delete($id){
          $this->db->delete($this->tablename(),array('id'=> $id));      
      }
      function findByID($id){
        $query = $this->db->get_where($this->tablename(), array('id' => $id));      
         return $query->row();
      }
      function findbyProject($id){
         $this->db->select($this->allFields(),false)->from($this->tablename())->where( array('projectid'=> $id) )->orderby('sdate');
        return $this->db->get()->result();
      }
      
      
      function getTotalBillableHours($projectid){
         $this->db->select_sum('hours')->from($this->tablename())->where( array('projectid'=> $projectid,'isbill'=>1) );        
         return $this->db->get()->row()->hours;  
           
         
      }
      
      function getTotalNonBillableHours($projectid){
        $this->db->select_sum('hours')->from($this->tablename())->where( array('projectid'=> $projectid,'isbill'=>0) );  
         return $this->db->get()->row()->hours;   
      }
  
  }
?>
